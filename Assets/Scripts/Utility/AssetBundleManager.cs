﻿using System.IO;
using UnityEngine;

public class AssetBundleManager : Singleton<AssetBundleManager> {
	protected AssetBundleManager () {}
	
	private AssetBundle _machineBundle, _partsBundle, _workersBundle, _modulesBundle;

	private void Awake()
	{
		_machineBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "prefabs/machines"));
		_partsBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "prefabs/parts"));
		_workersBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "prefabs/workers"));
		_modulesBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "prefabs/modules"));
		if (_machineBundle == null || _partsBundle == null || _workersBundle == null || _modulesBundle == null)
		{
			Debug.Log("Failed to load AssetBundle!");
		}
	}

	public AssetBundle GetMachineBundle()
	{
		return _machineBundle;
	}
	
	public AssetBundle GetPartsBundle()
	{
		return _partsBundle;
	}
	
	public AssetBundle GetWorkersBundle()
	{
		return _workersBundle;
	}
	
	public AssetBundle GetModulesBundle()
	{
		return _modulesBundle;
	}
	
}
