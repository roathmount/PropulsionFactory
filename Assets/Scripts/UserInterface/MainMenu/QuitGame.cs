﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour {

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        //Close the Play In Editor window if you are in editor
#else
        Application.Quit ();
        //Exit Application if Standalone/build version
#endif
    }
}
