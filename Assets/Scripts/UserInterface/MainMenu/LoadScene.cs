﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour {

    public void LoadByIndex(int SceneToLoad)
    {
        SceneManager.LoadScene(SceneToLoad);
        //Loads the Scene based on the SceneIndex in the Build Settings, 0 should be MianMenu and 1 should be MainGame
    }
}
