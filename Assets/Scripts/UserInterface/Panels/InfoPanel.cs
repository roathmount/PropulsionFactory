﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{

	private Text _titleCategory, _titleText;
	private Text _categoryTypes;
	private Text _amountPrice, _amountHours;
	private Text _descriptionTitle,_descriptionText;

	private void Awake()
	{
		foreach (var go in GameObject.FindGameObjectsWithTag("UIPanel"))
		{
			switch (go.name)
			{
				case "TitleText":
					_titleText = go.GetComponent<Text>();
					break;
				case "TitleCategory":
					_titleCategory = go.GetComponent<Text>();
					break;
				case "AmountPrice":
					_amountPrice = go.GetComponent<Text>();
					break;
				case "AmountHours":
					_amountHours = go.GetComponent<Text>();
					break;
				case "CategoryTypes":
					_categoryTypes = go.GetComponent<Text>();
					break;
				case "DescriptionTitle":
					_descriptionTitle = go.GetComponent<Text>();
					break;
				case "DescriptionText":
					_descriptionText = go.GetComponent<Text>();
					break;
				default:
					Debug.LogError("Error");
					break;
			}
		}
	}

	// Setters
	
	public void SetTitle(string data)
	{
		_titleText.text = data;
	}

	public void SetCategory(string data)
	{
		_titleCategory.text = data;
	}

	public void SetPrice(int amount)
	{
		_amountPrice.text = amount.ToString();
	}
	
	public void SetHours(int amount)
	{
		_amountHours.text = amount.ToString();
	}
	
	public void SetCategoryTypes(string data)
	{
		_categoryTypes.text = data;
	}
	
	public void SetDescriptionTitle(string data)
	{
		_descriptionTitle.text = data;
	}
	
	public void SetDescriptionText(string data)
	{
		_descriptionText.text = data;
	}
}
