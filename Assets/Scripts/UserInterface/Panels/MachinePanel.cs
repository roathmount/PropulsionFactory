﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum MachineTab
{
    Info,
    Orders,
    Actions,
    Stats
}

public class MachinePanel : MonoBehaviour
{
    public bool IsHovering{ get; set;}
    public bool IsSelected{ get; set;}

    private MachineTab _currentTab;

    public TextMeshProUGUI BasicMachineInfo, MachineName, MachineHealthAmount, MachineProgressAmount, MachineWearAmount;
    public TextMeshProUGUI CurrentTaskName,CurrentTaskPercentage;
    public Image MachineHealth,MachineProgress,MachineWear;

    private RectTransform _basicPanelTransform,_advPanelTransform,_canvasTransform;

    public GameObject BasicMachinePanel, AdvancedMachinePanel, CanvasGameObject;
    public GameObject InfoTab, OrdersTab, ActionsTab, StatsTab;
    public GameObject BlueprintList,QueueList,RecipeList;

    public Slider BarProgress;

    private Vector3 _selectionPoint;
    
    private Machine _currentMachine,_nextMachine;
    private Part _currentPart;

    private GameObject[] _blueprints,_partQueue,_recipeIngredients;
    
    private CameraWork _camera;

    private void Start()
    {
        _advPanelTransform = AdvancedMachinePanel.GetComponent<RectTransform>();
        _basicPanelTransform = BasicMachinePanel.GetComponent<RectTransform>();
        _canvasTransform = CanvasGameObject.GetComponent<RectTransform>();
        _currentTab = MachineTab.Info;

        if (Camera.main != null) _camera = Camera.main.gameObject.GetComponent<CameraWork>();
    }

    private void Update()
    {        
        if (IsSelected)
        {
            if (_currentMachine?.GetStoredPart() != null)
            {
                if (_currentPart == null)
                {
                    _currentPart = _currentMachine.GetStoredPart();
                    CurrentTaskName.text = _currentPart.GetPartData().GetName();
                    
                    LoadPartRecipe();
                }
                else if (_currentPart.ID != _currentMachine.GetStoredPart().ID)
                {
                    _currentPart = _currentMachine.GetStoredPart();
                    CurrentTaskName.text = _currentPart.GetPartData().GetName();
                    
                    LoadPartRecipe();
                }              
            }
            
            
            UpdateSelectedMode();
            
            // Keep UI panel near machine
            Vector2 panelPos = Camera.main.WorldToScreenPoint(_selectionPoint);
            Vector2 screen_pos = panelPos;
        
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _canvasTransform,
                panelPos,
                null,
                out screen_pos);
            _advPanelTransform.anchoredPosition =
                new Vector2(screen_pos.x + _advPanelTransform.sizeDelta.x - _camera.CurrZoom * 2,
                    screen_pos.y + _advPanelTransform.sizeDelta.y / 2 - _camera.CurrZoom * 2);

            // Scale UI panel based on camera zoom
            float scaleValue = ((80 - _camera.CurrZoom) / 48);
            _advPanelTransform.localScale= new Vector3(scaleValue,scaleValue,1);            
        }
        if (IsHovering)
        {
            BasicMachinePanel.transform.position = Input.mousePosition;
        }        
    }

    private void UpdateSelectedMode()
    {
        if (_currentTab == MachineTab.Info)
        {
            if (_currentMachine.GetManufacturerState() == ManufacturerState.Working)
            {
                float progress = _currentMachine.GetProgress();

                MachineProgressAmount.text = progress + " %";
                float position = Mathf.Lerp(0, 0.7f, progress / 100f);
                MachineProgress.fillAmount = position;
            }

            if (_currentMachine.GetStoredPart() == null)
            {
                LoadPartQueue();
            }
        }
        else if (_currentTab == MachineTab.Orders)
        {            
            if (_currentMachine.GetManufacturerState() == ManufacturerState.Working)
            {
                float progress = _currentMachine.GetProgress();

                CurrentTaskPercentage.text = (-1 * (_currentMachine.GetWorkAmount() - _currentMachine.TotalWorkAmount)) +
                                              "/" + _currentMachine.TotalWorkAmount;
                float position = Mathf.Lerp(0, 1f, progress / 100f);
                BarProgress.value = position;
            }
        }
    } 

    public void LoadBasicData(Machine machine)
    {
        _nextMachine = machine;
        BasicMachineInfo.text = _nextMachine.GetMachineData().GetName() + " (" + _nextMachine.GetWorkAmount() + ")";
    }

    public void LoadAdvancedData(Machine machine)
    {
        _currentMachine = machine;
        if (_nextMachine?.ID == _currentMachine.ID)
        {
            _nextMachine = null;
        }
        SetMachineName(_currentMachine.GetMachineData().GetName());
        MachineHealthAmount.text = _currentMachine.GetMachineData().GetHealth().ToString();
        MachineProgressAmount.text = _currentMachine.GetWorkAmount() + " %";
        MachineWearAmount.text = "0 %";
    }

    private void LoadBlueprints()
    {
        if (_blueprints != null && _blueprints.Length != 0)
        {
            foreach (var t in _blueprints)
            {
                Destroy(t);
            }
        }
        
        GameObject itemTemplate =  Resources.Load("Prefabs/UI/BlueprintItem") as GameObject;
        var parts = _currentMachine.GetMachineData().GetMachineParts();

        _blueprints = new GameObject[parts.Length];

        for (var i = 0; i < parts.Length; i++)
        {
            var part = parts[i];
            if ((int) part == 0) continue;
            GameObject itemClone = Instantiate(itemTemplate);
            itemClone.transform.SetParent(BlueprintList.transform);
            itemClone.GetComponent<Button>().onClick.AddListener(delegate { OnMachinePartSelected((int)part); });
            
            itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.ToString();
            _blueprints[i] = itemClone;
        }
    }
    
    private void LoadPartQueue()
    {
        if ((_partQueue != null && _partQueue.Length != 0))
        {
            foreach (var t in _partQueue)
            {
                Destroy(t);
            }
        }
        
        if(_currentMachine.GetStoredPart() == null) { return; }
                
        _partQueue = new GameObject[1];

        GameObject itemTemplate =  Resources.Load("Prefabs/UI/BlueprintItem") as GameObject;
        var part = _currentMachine.GetStoredPart();

        GameObject itemClone = Instantiate(itemTemplate);
        itemClone.transform.SetParent(QueueList.transform);
        itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.GetPartData().GetName() + " (x"+part.GetPartData().GetAmountLimit()+")";
        _partQueue[0] = itemClone;

        // TODO: enable this for queue system
        /*
        _partQueue = new GameObject[parts.Length];

        for (var i = 0; i < parts.Length; i++)
        {
            var part = parts[i];
            if ((int) part == 0) continue;
            GameObject itemClone = Instantiate(itemTemplate);
            itemClone.transform.SetParent(BlueprintList.transform);
            itemClone.GetComponent<Button>().onClick.AddListener(delegate { OnMachinePartSelected((int)part); });
            
            itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.ToString();
            _partQueue[i] = itemClone;
        }
        */
    }

    private void LoadPartRecipe()
    {
        if (_recipeIngredients != null && _recipeIngredients.Length > 0)
        {
            foreach (var ingredient in _recipeIngredients)
            {
                Destroy(ingredient);
            }
        }
        
        _recipeIngredients = new GameObject[_currentMachine.GetRecipe().GetItems().Count];

        var list = _currentMachine.GetRecipe().GetItems();
        for (var i = 0; i < list.Count; i++)
        {
            RecipeItem item = list[i];
            GameObject itemText = Instantiate(Resources.Load("Prefabs/UI/IngredientText") as GameObject);
            itemText.transform.SetParent(RecipeList.transform);

            itemText.GetComponent<TextMeshProUGUI>().SetText(item.GetItemType() + " (" + item.GetItemAmount() + ")");

            _recipeIngredients[i] = itemText;
        }
    }
    
    public void ShowBasicMenu()
    {
        if (!BasicMachinePanel.activeInHierarchy)
        {
            IsHovering = true;
            BasicMachinePanel.transform.position = Input.mousePosition;
            BasicMachinePanel.SetActive(true);
        }
        else
        {
            return;
        }
    }

    public void ShowAdvancedMenu(Vector3 selectionPoint)
    {
        _selectionPoint = selectionPoint;
        
        if (!AdvancedMachinePanel.activeInHierarchy)
        {
            if (BasicMachinePanel.activeInHierarchy)
            {
                DisableBasicPanel();
            }

            IsSelected = true;
            Vector2 panelPos = Camera.main.WorldToScreenPoint(_selectionPoint);
            Vector2 screen_pos = panelPos;
        
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _canvasTransform,
                panelPos,
                null,
                out screen_pos);
            _advPanelTransform.anchoredPosition = new Vector2(screen_pos.x + _advPanelTransform.sizeDelta.x, screen_pos.y + _advPanelTransform.sizeDelta.y/2);
            AdvancedMachinePanel.SetActive(true);
            // Always open the info tab first
            EnableTab(MachineTab.Info);            
        }
        else
        {
            return;
        }
    }
        
    public void DisableBasicPanel()
    {
        IsHovering = false;
        _nextMachine = null;
        BasicMachinePanel.SetActive(false);
    }
    
    public void DisableAdvancedPanel()
    {
        IsSelected = false;
        _currentMachine = null;
        DisableTab(_currentTab);
        AdvancedMachinePanel.SetActive(false);
    }

    public void EnableTab(int tabType)
    {
        DisableTab(_currentTab);
        switch (tabType)
        {
            case 0:
                InfoTab.SetActive(true);
                LoadPartQueue();
                _currentTab = MachineTab.Info;
                break;
            case 1:
                OrdersTab.SetActive(true);
                LoadBlueprints();
                _currentTab = MachineTab.Orders;
                break;
            case 2:
                ActionsTab.SetActive(true);
                _currentTab = MachineTab.Actions;
                break;
            case 3:
                StatsTab.SetActive(true);
                _currentTab = MachineTab.Stats;
                break;
        }
    }
    
    // TODO: move each tab start to a separated function
    public void EnableTab(MachineTab tabType)
    {
        DisableTab(_currentTab);  
        switch (tabType)
        {
            case MachineTab.Info:
                InfoTab.SetActive(true);
                LoadPartQueue();
                break;
            case MachineTab.Orders:
                OrdersTab.SetActive(true);
                LoadBlueprints();
                break;
            case MachineTab.Actions:
                ActionsTab.SetActive(true);
                break;
            case MachineTab.Stats:
                StatsTab.SetActive(true);
                break;
        }
        _currentTab = tabType;
    }
    
    public void DisableTab(MachineTab tabType)
    {
        switch (tabType)
        {
            case MachineTab.Info:
                InfoTab.SetActive(false);
                break;
            case MachineTab.Orders:
                OrdersTab.SetActive(false);
                break;
            case MachineTab.Actions:
                ActionsTab.SetActive(false);
                break;
            case MachineTab.Stats:
                StatsTab.SetActive(false);
                break;
        }
    }

    public void OnMachinePartSelected(int part)
    {
        Messenger<PartType, int>.Broadcast("new MachinePartSelected", (PartType) part, _currentMachine.ID);
    }
    
    public string GetMachineName()
    {
        return MachineName.text;
    }

    private void SetMachineName(string value)
    {
        MachineName.text = value;
    }

    public GridObject GetCurrentMachine()
    {
        return _currentMachine;
    }
    
    public GridObject GetNextMachine()
    {
        return _nextMachine;
    }    
}