﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemPanel : MonoBehaviour
{

	public bool IsHovering{ get; set;}
	public bool IsSelected{ get; set;}

	public GameObject BasicItemPanel, AdvancedItemPanel,CanvasGameObject;
	
	public TextMeshProUGUI BasicItemInfo, ItemName, ItemCategory, ItemQuality, ItemAmount, ItemSize;
	public Image ItemCategoryIcon;
	
	private RectTransform _basicPanelTransform,_advPanelTransform,_canvasTransform;

	private Vector3 _selectionPoint;

	private GridObject _currentItem,_nextItem;

	private void Start()
	{
		_advPanelTransform = AdvancedItemPanel.GetComponent<RectTransform>();
		_basicPanelTransform = BasicItemPanel.GetComponent<RectTransform>();
		_canvasTransform = CanvasGameObject.GetComponent<RectTransform>();
	}

	private void Update()
	{        
		if (IsSelected)
		{   
			Vector2 panelPos = Camera.main.WorldToScreenPoint(_selectionPoint);
			Vector2 screen_pos = panelPos;
        
			RectTransformUtility.ScreenPointToLocalPointInRectangle(
				_canvasTransform,
				panelPos,
				null,
				out screen_pos);
			_advPanelTransform.anchoredPosition = new Vector2(screen_pos.x + _advPanelTransform.sizeDelta.x*1.5f, screen_pos.y);
		}
		if (IsHovering)
		{
			BasicItemPanel.transform.position = Input.mousePosition;
		}        
	}
	
	public void LoadBasicData(GridObject item, PartData data, int itemAmount)
	{
		_nextItem = item;
		BasicItemInfo.text = data.GetName() + " (" + itemAmount.ToString() + ")";
	}
	
	public void LoadAdvancedData(GridObject item, PartData data, Quality itemQuality, int itemAmount)
	{
		_currentItem = item;
		SetItemName(data.GetName());
		SetItemCategory(_currentItem.GetGridObjectType().ToString());
		SetItemCategoryIcon(Resources.Load<Sprite>("Prefabs/UI/ItemCategoryIcons/" +_currentItem.GetGridObjectType().ToString()+"Icon"));
		SetItemQuality(itemQuality.ToString());
		SetItemSize(data.GetSize());
		// Item amount changes multiple times, so its updated each time the value changes
		SetItemAmount(itemAmount);
	}

	public void ShowBasicMenu()
	{
		if (!BasicItemPanel.activeInHierarchy)
		{
			IsHovering = true;
			BasicItemPanel.transform.position = Input.mousePosition;
			BasicItemPanel.SetActive(true);
		}
		else
		{
			return;
		}
	}

	public void ShowAdvancedMenu(Vector3 selectionPoint)
	{
		_selectionPoint = selectionPoint;
        
		if (!AdvancedItemPanel.activeInHierarchy)
		{
			if (BasicItemPanel.activeInHierarchy)
			{
				DisableBasicPanel();
			}

			IsSelected = true;
			AdvancedItemPanel.SetActive(true);
		}
		else
		{
			return;
		}
	}

	public void DisableBasicPanel()
	{
		IsHovering = false;
		_nextItem = null;
		BasicItemPanel.SetActive(false);
	}
    
	public void DisableAdvancedPanel()
	{
		IsSelected = false;
		_currentItem = null;
		AdvancedItemPanel.SetActive(false);
	}
	

	public GridObject GetCurrentItem()
	{
		return _currentItem;
	}

	public GridObject GetNextItem()
	{
		return _nextItem;
	}
		
	public string GetItemName()
	{
		return ItemName.text;
	}
	
	public string GetItemCategory()
	{
		return ItemCategory.text;
	}

	public Sprite GetItemCategoryIcon()
	{
		return ItemCategoryIcon.sprite;
	}
	
	public string GetItemQuality()
	{
		return ItemQuality.text;
	}
	
	public string GetItemAmount()
	{
		return ItemAmount.text;
	}
	
	public string GetItemSize()
	{
		return ItemSize.text;
	}

	public void SetCurrentTarget(GridObject target)
	{
		_currentItem = target;
	}

	public void SetItemName(string value)
	{
		ItemName.text = value;
	}
	
	public void SetItemCategory(string value)
	{
		ItemCategory.text = value;
	}

	public void SetItemCategoryIcon(Sprite image)
	{
		ItemCategoryIcon.overrideSprite = image;
	}

	public void SetItemQuality(string value)
	{
		ItemQuality.text = value +" quality " + GetItemCategory().ToLower();
	}

	public void SetItemAmount(int value)
	{
		ItemAmount.text = "x"+value.ToString();
	}
	
	public void SetItemSize(Vector2 value)
	{
		ItemSize.text = value.x.ToString() + " x " + value.y.ToString();
	}
}
