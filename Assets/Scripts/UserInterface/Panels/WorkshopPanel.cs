﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum WorkshopTab
{
    Info,
    Orders,
    Staff,
    Modules,
    Actions,
    Stats
}

public class WorkshopPanel : MonoBehaviour
{

    private bool _isHovering,_isSelected;

    private WorkshopTab _currentTab;
    
    private Vector3 _selectionPoint;

    public TextMeshProUGUI WorkshopName, WorkshopType, WorkshopIntegrityAmount, WorkshopProgressAmount;
    public TextMeshProUGUI CurrentTaskName,CurrentTaskPercentage;
    public Image WorkshopIntegrityBar,WorkshopProgressBar;


    private RectTransform _workshopPanelTransform,_canvasTransform;
    
    public GameObject WorkshopPanelGo, CanvasGo;
    public GameObject InfoTab, OrdersTab, StaffTab, ModulesTab, ActionsTab, StatsTab;
    public GameObject BlueprintList,QueueList,RecipeList;
    private GameObject[] _blueprints,_partQueue,_recipeIngredients;

    private CameraWork _camera;
    
    public Slider BarProgress;

    private Workshop _currentWorkshop;
    private Part _currentPart;

    public bool IsHovering
    {
        get { return _isHovering; }
        set { _isHovering = value; }
    }

    public bool IsSelected
    {
        get { return _isSelected; }
        set { _isSelected = value; }
    }

    private void Start()
    {
        _workshopPanelTransform = WorkshopPanelGo.GetComponent<RectTransform>();
        _canvasTransform = CanvasGo.GetComponent<RectTransform>();

        _currentTab = WorkshopTab.Info;
        
        if (Camera.main != null) _camera = Camera.main.gameObject.GetComponent<CameraWork>();
    }

    private void Update()
    {
        if (IsSelected)
        {
            if (_currentWorkshop?.GetStoredPart() != null)
            {
                if (_currentPart == null)
                {
                    _currentPart = _currentWorkshop.GetStoredPart();
                    CurrentTaskName.text = _currentPart.GetPartData().GetName();
                    
                    LoadPartRecipe();
                }
                else if (_currentPart.ID != _currentWorkshop.GetStoredPart().ID)
                {
                    _currentPart = _currentWorkshop.GetStoredPart();
                    CurrentTaskName.text = _currentPart.GetPartData().GetName();
                    
                    LoadPartRecipe();
                }              
            }
            
            UpdateSelectedMode();
            
            // Keep UI panel near workshop
            Vector2 panelPos = Camera.main.WorldToScreenPoint(_selectionPoint);
            Vector2 screen_pos = panelPos;
        
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _canvasTransform,
                panelPos,
                null,
                out screen_pos);
            _workshopPanelTransform.anchoredPosition =
                new Vector2(screen_pos.x + _workshopPanelTransform.sizeDelta.x - _camera.CurrZoom * 2,
                    screen_pos.y + _workshopPanelTransform.sizeDelta.y / 2 - _camera.CurrZoom * 2);

            // Scale UI panel based on camera zoom
            float scaleValue = ((80 - _camera.CurrZoom) / 48);
            _workshopPanelTransform.localScale= new Vector3(scaleValue,scaleValue,1);            
        }
    }
    
    private void UpdateSelectedMode()
    {
        if (_currentTab == WorkshopTab.Info)
        {
            if (_currentWorkshop.GetManufacturerState() == ManufacturerState.Working)
            {

                float progress = _currentWorkshop.GetProgress();

                WorkshopProgressAmount.text = progress + " %";
                float position = Mathf.Lerp(0, 0.7f, progress / 100f);
                WorkshopProgressBar.fillAmount = position;
            }

            if (_currentWorkshop.GetStoredPart() == null)
            {
                LoadPartQueue();
            }
        }
        else if (_currentTab == WorkshopTab.Orders)
        {            
            if (_currentWorkshop.GetManufacturerState() == ManufacturerState.Working)
            {
                float progress = _currentWorkshop.GetProgress();

                CurrentTaskPercentage.text = (-1 * (_currentWorkshop.GetWorkAmount() - _currentWorkshop.TotalWorkAmount)) +
                                             "/" + _currentWorkshop.TotalWorkAmount;
                float position = Mathf.Lerp(0, 1f, progress / 100f);
                BarProgress.value = position;
            }
        }
    } 

    public void LoadData(Workshop workshop)
    {
        _currentWorkshop = workshop;

        WorkshopName.text = _currentWorkshop.GetWorkshopData().GetName();
        WorkshopType.text = _currentWorkshop.GetWorkshopData().GetWorkshopType().ToString();
    }
    
    private void LoadBlueprints()
    {
        if (_blueprints != null && _blueprints.Length != 0)
        {
            foreach (var t in _blueprints)
            {
                Destroy(t);
            }
        }
        
        GameObject itemTemplate =  Resources.Load("Prefabs/UI/BlueprintItem") as GameObject;
        var parts = _currentWorkshop.GetWorkshopData().GetWorkshopParts();

        _blueprints = new GameObject[parts.Length];

        for (var i = 0; i < parts.Length; i++)
        {
            var part = parts[i];
            if ((int) part == 0) continue;
            GameObject itemClone = Instantiate(itemTemplate);
            itemClone.transform.SetParent(BlueprintList.transform);
            itemClone.GetComponent<Button>().onClick.AddListener(delegate { OnWorkshopPartSelected((int) part); });
            
            itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.ToString();
            _blueprints[i] = itemClone;
        }
    }
    
    private void LoadPartQueue()
    {
        if ((_partQueue != null && _partQueue.Length != 0))
        {
            foreach (var t in _partQueue)
            {
                Destroy(t);
            }
        }
        
        if(_currentWorkshop.GetStoredPart() == null) { return; }
                
        _partQueue = new GameObject[1];

        GameObject itemTemplate =  Resources.Load("Prefabs/UI/BlueprintItem") as GameObject;
        var part = _currentWorkshop.GetStoredPart();

        GameObject itemClone = Instantiate(itemTemplate);
        itemClone.transform.SetParent(QueueList.transform);
        itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.GetPartData().GetName() + " (x"+part.GetPartData().GetAmountLimit()+")";
        _partQueue[0] = itemClone;

        // TODO: enable this for queue system
        /*
        _partQueue = new GameObject[parts.Length];

        for (var i = 0; i < parts.Length; i++)
        {
            var part = parts[i];
            if ((int) part == 0) continue;
            GameObject itemClone = Instantiate(itemTemplate);
            itemClone.transform.SetParent(BlueprintList.transform);
            itemClone.GetComponent<Button>().onClick.AddListener(delegate { OnMachinePartSelected((int)part); });
            
            itemClone.transform.GetChild(0).GetComponent<TMP_Text>().text = part.ToString();
            _partQueue[i] = itemClone;
        }
        */
    }

    private void LoadPartRecipe()
    {
        if (_recipeIngredients != null && _recipeIngredients.Length > 0)
        {
            foreach (var ingredient in _recipeIngredients)
            {
                Destroy(ingredient);
            }
        }
        
        _recipeIngredients = new GameObject[_currentWorkshop.GetRecipe().GetItems().Count];

        var list = _currentWorkshop.GetRecipe().GetItems();
        for (var i = 0; i < list.Count; i++)
        {
            RecipeItem item = list[i];
            GameObject itemText = Instantiate(Resources.Load("Prefabs/UI/IngredientText") as GameObject);
            itemText.transform.SetParent(RecipeList.transform);

            itemText.GetComponent<TextMeshProUGUI>().SetText(item.GetItemType() + " (" + item.GetItemAmount() + ")");

            _recipeIngredients[i] = itemText;
        }
    }

    public void ShowPanel(Vector3 selectionPoint)
    {
        _selectionPoint = selectionPoint;

        IsSelected = true;
        Vector2 panelPos = Camera.main.WorldToScreenPoint(_selectionPoint);
        Vector2 screen_pos = panelPos;
        
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            _canvasTransform,
            panelPos,
            null,
            out screen_pos);
        _workshopPanelTransform.anchoredPosition = new Vector2(screen_pos.x + _workshopPanelTransform.sizeDelta.x,
            screen_pos.y + _workshopPanelTransform.sizeDelta.y / 2);
        WorkshopPanelGo.SetActive(true);
        // Opening the info tab by default
        EnableTab(WorkshopTab.Info);
    }

    public void DisablePanel()
    {
        IsSelected = false;
        _currentWorkshop = null;
        DisableTab(_currentTab);
        WorkshopPanelGo.SetActive(false);
    }
    
    public void EnableTab(int tabType)
    {
        DisableTab(_currentTab);
        switch (tabType)
        {
            case 0:
                InfoTab.SetActive(true);
                LoadPartQueue();
                _currentTab = WorkshopTab.Info;
                break;
            case 1:
                OrdersTab.SetActive(true);
                LoadBlueprints();
                _currentTab = WorkshopTab.Orders;
                break;
            case 2:
                StaffTab.SetActive(true);
                _currentTab = WorkshopTab.Staff;
                break;
            case 3:
                ModulesTab.SetActive(true);
                _currentTab = WorkshopTab.Modules;
                break;
            case 4:
                ActionsTab.SetActive(true);
                _currentTab = WorkshopTab.Actions;
                break;
            case 5:
                StatsTab.SetActive(true);
                _currentTab = WorkshopTab.Stats;
                break;
        }
    }
    
    // TODO: move each tab start to a separated function
    public void EnableTab(WorkshopTab tabType)
    {
        DisableTab(_currentTab);  
        switch (tabType)
        {
            case WorkshopTab.Info:
                InfoTab.SetActive(true);
                LoadPartQueue();
                break;
            case WorkshopTab.Orders:
                OrdersTab.SetActive(true);
                LoadBlueprints();
                break;
            case WorkshopTab.Staff:
                StaffTab.SetActive(true);
                break;
            case WorkshopTab.Modules:
                ModulesTab.SetActive(true);
                break;
            case WorkshopTab.Actions:
                ActionsTab.SetActive(true);
                break;
            case WorkshopTab.Stats:
                StatsTab.SetActive(true);
                break;
        }
        _currentTab = tabType;
    }
    
    public void DisableTab(WorkshopTab tabType)
    {
        switch (tabType)
        {
            case WorkshopTab.Info:
                InfoTab.SetActive(false);
                break;
            case WorkshopTab.Orders:
                OrdersTab.SetActive(false);
                break;
            case WorkshopTab.Staff:
                StaffTab.SetActive(false);
                break;
            case WorkshopTab.Modules:
                ModulesTab.SetActive(false);
                break;
            case WorkshopTab.Actions:
                ActionsTab.SetActive(false);
                break;
            case WorkshopTab.Stats:
                StatsTab.SetActive(false);
                break;
        }
    }
    
    public void OnWorkshopPartSelected(int part)
    {
        Messenger<Part, PartType, int>.Broadcast("new WorkshopPartSelected", null, (PartType) part, _currentWorkshop.ID);
    }
    
    public GridObject GetCurrentWorkshop()
    {
        return _currentWorkshop;
    }
}
