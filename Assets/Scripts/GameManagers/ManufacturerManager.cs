﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ManufacturerManager : MonoBehaviour
{
    private List<Machine> _machines;
    private List<Workshop> _workshops;
    
    private GridManager _gridManager;
    private TaskManager _taskManager;
    
    private PartDataList _partDatas;
    private RecipeList _recipeList;

    private ModuleDataList _moduleDatas;

    private Machine _partDispenser,_resourceDispenser;
    
    private void Start()
    {
        _machines = new List<Machine>();
        _workshops = new List<Workshop>();
        
        _gridManager = GetComponent<GridManager>();
        _taskManager = GetComponent<TaskManager>();
        
        _partDatas = new PartDataList();
        TextAsset partDataFile = Resources.Load<TextAsset>("Parts");
        _partDatas = JsonUtility.FromJson<PartDataList>(partDataFile.text);
        
        _recipeList = new RecipeList();
        TextAsset recipeListFile = Resources.Load<TextAsset>("Recipes");
        _recipeList = JsonUtility.FromJson<RecipeList>(recipeListFile.text);
        
        _moduleDatas = new ModuleDataList();
        TextAsset moduleDataFile = Resources.Load<TextAsset>("Modules");
        _moduleDatas = JsonUtility.FromJson<ModuleDataList>(moduleDataFile.text);
        
        Messenger<Machine>.AddListener("new ManufacturerRequestOperator", OnMachineRequestOperator);
        
        Messenger<PartType, int>.AddListener("new MachinePartSelected", OnMachinePartSelected);
        Messenger<Part, PartType, int>.AddListener("new WorkshopPartSelected", OnWorkshopPartSelected);

        Messenger<Manufacturer>.AddListener("new ManufacturerRequestWithdraw", OnManufacturerRequestWithdraw);
        Messenger<Manufacturer, RecipeItem>.AddListener("new ManufacturerRequestRecharge", OnManufacturerRequestRecharge);

        StartCoroutine(TickMachineProduction());
    }
    
    private IEnumerator TickMachineProduction()
    {
        for (;;)
        {
            foreach (var machine in _machines)
            {
                // TODO: remove dispensers as machines?
                if (machine.GetMachineData().GetMachineType() != ManufacturerType.Dispenser) // Look on every machine that isn't a dispenser
                {
                    switch (machine.GetManufacturerState()) // See on what state each machine is
                    {
                        case ManufacturerState.Idle: // Machine is in idle state

                            break;
                        case ManufacturerState.Working: // Machine is in working state
                            machine.Work();

                            break;
                        case ManufacturerState.Waiting:
                            if (machine.IsEmpty() && machine.GetStoredPart() == null)
                            {
                                OnMachinePartSelected(machine.GetRecipe().GetPartType(),machine.ID);
                            }
                            break;
                        case ManufacturerState.Repairing:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            
            foreach (var workshop in _workshops)
            {
                switch (workshop.GetManufacturerState()) // See on what state each machine is
                {
                    case ManufacturerState.Idle: // Machine is in idle state

                        break;
                    case ManufacturerState.Working: // Machine is in working state
                        workshop.Work();

                        break;
                    case ManufacturerState.Waiting:
                        if (!workshop.IsMultipleProduction() && workshop.IsEmpty() && workshop.GetStoredPart() == null)
                        {
                            OnWorkshopPartSelected(null, workshop.GetRecipe().GetPartType(),workshop.ID);
                        }
                        break;
                    case ManufacturerState.Repairing:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public PartData GetPartData(PartType type)
    {
        return _partDatas.GetPart(type);
    }

    public ModuleData GetModuleData(ModuleType type)
    {
        return _moduleDatas.GetModule(type);
    }
    
    public Machine GetPartDispenser()
    {
        if (_partDispenser == null)
        {
            foreach (var machine in _machines)
            {
                if (machine.GetMachineData().GetMachineType() == ManufacturerType.Dispenser && machine != _resourceDispenser)
                {
                    _partDispenser = machine;
                }
            }
        }
        return _partDispenser;
    }
    
    public Machine GetResourceDispenser()
    {
        if (_resourceDispenser == null)
        {
            foreach (var machine in _machines)
            {
                if (machine.GetMachineData().GetMachineType() == ManufacturerType.Dispenser && machine != _partDispenser)
                {
                    _resourceDispenser = machine;
                }
            }
        }
        return _resourceDispenser;
    }
    
    // When a new machine is built its added here
    public void AddMachine(Machine machine)
    {
        _machines.Add(machine);        
    }
    
    // When a new workshop is defined add it here
    public void AddWorkshop(Workshop workshop)
    {
        _workshops.Add(workshop);
    }

    public Machine GetMachine(int id)
    {
        return _machines.FirstOrDefault(machine => machine.ID == id);
    }
    
    public Workshop GetWorkshop(int id)
    {
        return _workshops.FirstOrDefault(workshop => workshop.ID == id);
    }

    // Event system method for selected part to manufacture, using recipes
    private void OnMachinePartSelected(PartType part, int machineID)
    {
        // Get the machine
        Machine selectedMachine = GetMachine(machineID);
        // Look for selected part recipe
        selectedMachine.ClearResources();

        var newList = _recipeList.GetPartRecipe(part).GetItems().Select(item => new RecipeItem(item.GetItemType(), item.GetItemAmount(), item.RequieresItemFinished())).ToList();

        Recipe usedRecipe = new Recipe(_recipeList.GetPartRecipe(part).GetPartType(),newList);
        
        if (_recipeList != null)
        {
            if (usedRecipe.GetPartType() != PartType.None)
            {
                // Add the recipe item parts to the machine list
                foreach (var item in usedRecipe.GetItems())
                {
                    // Create a empty part for this item
                    PartData oldData = _partDatas.GetPart(item.GetItemType());
                    PartData resourceData = new PartData(oldData.GetName(), oldData.GetHealth(), item.GetItemAmount(),
                        oldData.GetPartType(), oldData.IsResource(), oldData.GetSize(), oldData.GetDescription());

                    Part resource = new Part(resourceData, 0, selectedMachine.X, selectedMachine.Y);
                    
                    // Add part to resource list
                    selectedMachine.AddResource(resource);
                }
            }
        }
        // Start manufactuing process on machine, sending the part and the recipe
        selectedMachine.ManufacturePart(_partDatas.GetPart(part), usedRecipe);
    }
    
    private void OnWorkshopPartSelected(Part part, PartType partType, int workshopID)
    {
        // Get the machine
        Workshop selectedWorkshop = GetWorkshop(workshopID);
        
        if (part != null)
        {
            var newRecipeItemList = _recipeList.GetPartRecipe(partType).GetItems().Select(item =>
                new RecipeItem(item.GetItemType(), item.GetItemAmount(), item.RequieresItemFinished())).ToList();

            Recipe usedRecipe = new Recipe(_recipeList.GetPartRecipe(partType).GetPartType(),newRecipeItemList);
            
            if (_recipeList != null)
            {
                if (usedRecipe.GetPartType() != PartType.None)
                {
                    // Add the recipe item parts to the machine list
                    foreach (var item in usedRecipe.GetItems())
                    {
                        if (selectedWorkshop.GetResource(item.GetItemType()) == null)
                        {
                            // Create a empty part for this item
                            PartData oldData = _partDatas.GetPart(item.GetItemType());
                            PartData resourceData = new PartData(oldData.GetName(), oldData.GetHealth(),
                                item.GetItemAmount(), oldData.GetPartType(), oldData.IsResource(), oldData.GetSize(),
                                oldData.GetDescription());

                            Part resource = new Part(resourceData, 0, selectedWorkshop.X, selectedWorkshop.Y);

                            // Add part to resource list
                            selectedWorkshop.AddResource(resource);
                        }
                    }
                }
            }
            
            selectedWorkshop.ManufactureProduct(part, usedRecipe); 
        }
        else
        {
            // Look for selected part recipe
            selectedWorkshop.ClearResources();

            var newRecipeItemList = _recipeList.GetPartRecipe(partType).GetItems().Select(item =>
                new RecipeItem(item.GetItemType(), item.GetItemAmount(), item.RequieresItemFinished())).ToList();

            Recipe usedRecipe = new Recipe(_recipeList.GetPartRecipe(partType).GetPartType(),newRecipeItemList);
        
            if (_recipeList != null)
            {
                if (usedRecipe.GetPartType() != PartType.None)
                {
                    // Add the recipe item parts to the machine list
                    foreach (var item in usedRecipe.GetItems())
                    {
                        // Create a empty part for this item
                        PartData oldData = _partDatas.GetPart(item.GetItemType());
                        PartData resourceData = new PartData(oldData.GetName(), oldData.GetHealth(),
                            item.GetItemAmount(),
                            oldData.GetPartType(), oldData.IsResource(), oldData.GetSize(), oldData.GetDescription());

                        Part resource = new Part(resourceData, 0, selectedWorkshop.X, selectedWorkshop.Y);
                    
                        // Add part to resource list
                        selectedWorkshop.AddResource(resource);
                    }
                }
            }
            // Start manufactuing process on machine, sending the part and the recipe
            selectedWorkshop.ManufacturePart(_partDatas.GetPart(partType), usedRecipe); 
        }
    }
    
    private void OnManufacturerRequestWithdraw(Manufacturer manufacturer)
    {
        _taskManager.GenerateTask(TaskType.TransportGridObject,manufacturer);
    }

    private void OnManufacturerRequestRecharge(Manufacturer manufacturer, RecipeItem item)
    {
        _taskManager.GenerateTask(TaskType.RechargeManufacturer, manufacturer, item, item.GetItemAmount());
    }
    
    // Machine request for a worker to operate it and manufacture
    private void OnMachineRequestOperator(Machine machine)
    {
        _taskManager.GenerateTask(TaskType.OperateManufacturer, machine);
    }
    
    public bool IsOverlapping(Vector3 pos, Machine machine)
    {        
        // getting the first tile from the machine
        int posX = (int) pos.x - ((int) (machine.GetMachineData().GetSize().x / 2));
        int posY = (int) pos.z - ((int) (machine.GetMachineData().GetSize().y / 2));
		
        // looking into each tile and adding it to the list
        for (int x = posX; x < machine.GetMachineData().GetSize().x + posX; x++ )
        {
            for (int y = posY; y < machine.GetMachineData().GetSize().y + posY; y++)
            {
                GridTile tile = _gridManager.GetTile(x, y);
                if (tile != null)
                {
                    if (!tile.Passable || tile.Service || tile.Reserved)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public bool IsOverlapping(Vector3 pos, Part part)
    {        
        // getting the first tile from the part
        int posX = (int) pos.x - ((int) (part.GetPartData().GetSize().x / 2));
        int posY = (int) pos.z - ((int) (part.GetPartData().GetSize().y / 2));
		
        // looking into each tile and adding it to the list
        for (int x = posX; x < part.GetPartData().GetSize().x + posX; x++ )
        {
            for (int y = posY; y < part.GetPartData().GetSize().y + posY; y++)
            {
                GridTile tile = _gridManager.GetTile(x, y);
                if (tile != null)
                {
                    if (!tile.Passable || tile.Service)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public bool IsOverlapping(Vector3 pos, Module mod)
    {        
        // getting the first tile from the part
        int posX = (int) pos.x - ((int) (mod.GetModuleData().GetSize().x / 2));
        int posY = (int) pos.z - ((int) (mod.GetModuleData().GetSize().y / 2));
		
        // looking into each tile and adding it to the list
        for (int x = posX; x < mod.GetModuleData().GetSize().x + posX; x++ )
        {
            for (int y = posY; y < mod.GetModuleData().GetSize().y + posY; y++)
            {
                GridTile tile = _gridManager.GetTile(x, y);
                if (tile != null)
                {
                    if (!tile.Passable || tile.Service)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public bool IsOverlapping(Vector3 pos, Vector2 size)
    {        
        // getting the first tile from the machine
        int posX = (int) pos.x;
        int posY = (int) pos.z;
                
        // looking into each tile and adding it to the list
        for (int x = posX; x < size.x + posX; x++ )
        {
            for (int y = posY; y < size.y + posY; y++)
            {
                GridTile tile = _gridManager.GetTile(x, y);
                if (tile != null)
                {
                    if (!tile.Passable || tile.Service || tile.Reserved)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public bool IsReverseOverlapping(Vector3 pos, Vector2 size, Manufacturer target)
    {        
        // getting the first tile from the machine
        int posX = (int) pos.x - ((int) (size.x / 2));
        int posY = (int) pos.z - ((int) (size.y / 2));
		        
        // looking into each tile and adding it to the list
     
        for (int x = posX; x < posX + size.x; x++ )
        {
            for (int y = posY; y < posY + size.y; y++)
            {
                GridTile tile = _gridManager.GetTile(x, y);
                if (tile != null)
                {
                    if (!target.IsTileReserved(tile.ID))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // fuction used to remove machines from in-game and entity
    public void RemoveMachine(int id)
    {
        foreach (var machine in _machines) // looking into each machine for the one requested
        {
            if (machine.ID != id) continue;
            machine.Remove();
            _machines.Remove(machine);
            break;
        }
    }
    
    // TODO: proper workshop removal
    public void RemoveWorkshop(int id)
    {
        foreach (var workshop in _workshops) // looking into each workshop for the one requested
        {
            if (workshop.ID != id) continue;
            workshop.Remove();
            _workshops.Remove(workshop);
            break;
        }
    }
    
}
