﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridManager : MonoBehaviour
{

	private Dictionary<Point, GridTile> _grid;

	public static readonly int SizeX = 200;
	public static readonly int SizeY = 200;

	private void Awake () {
		
		_grid = new Dictionary<Point, GridTile>();
		
		for (int id = 0, y = 0; y < SizeY; y++)
		{
			for (int x = 0; x < SizeX; x++, id++)
			{
				GridTile tile = new GridTile(id,x,y);
				_grid.Add(tile.Location,tile);
			}
		}
	}

	private void Update () {
		
	}

	public int GetTileID(int x, int y)
	{
		return (SizeX * x) + y;
	}

	public GridTile GetTilePos(int id)
	{
		return _grid.Values.FirstOrDefault(tile => tile.ID == id);
	}

	public GridTile GetTile(int x, int y)
	{
		if (x >= 0 && x < SizeX && y < SizeY && y >= 0)
		{
			var tile = _grid[new Point(x, y)];
			if (tile != null)
			{
				return tile;
			}
		}
		return null;
	}

	public Vector2 GridSize()
	{
		return new Vector2(SizeX,SizeY);
	}
}
