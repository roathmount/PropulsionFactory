﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GridObjectManager : MonoBehaviour
{

    private ManufacturerManager _manufacturerManager;

    private List<Stockpile> _stockpiles;
    private List<Part> _parts;

    private void Awake()
    {
        _manufacturerManager = GetComponent<ManufacturerManager>();
        
        _stockpiles = new List<Stockpile>();
        _parts = new List<Part>();
        
        Messenger<Part>.AddListener("new OnRemovePart", RemovePart);

    }

    private void Start()
    {
    }

    private void Update()
    {
    }
    
    // Getters
    
    // Return a part based on type
    public Part RequestPart(PartType requestedPartType)
    {
        return _parts.FirstOrDefault(res => res.GetPartData().GetPartType() == requestedPartType);
    }
    
    public Part RequestClosestPart(PartType requestedPartType,int amount, Point destination)
    {
        Part bestPart = null;
        if (_parts.Count > 0)
        {
            foreach (var part in _parts)
            {
                if (part.Location.X == 0 && part.Location.Y == 0 || part.GetPartData().GetPartType() != requestedPartType || part.IsQueued() ) continue;
                if (part.GetPartAmount() <= 0) continue;
                if (bestPart != null)
                {
                    if (Point.Distance(destination, part.Location) <
                        Point.Distance(destination, bestPart.Location))
                    {
                        bestPart = part;
                    }
                }
                else
                {
                    bestPart = part;
                }
            }
        }
        return bestPart;
    }

    // TODO: return stockpile based on configuration
    public Stockpile RequeStockpileOf(Point position)
    {
        if (_stockpiles.Count > 0)
        {
            Stockpile bestStockpile = null;
            foreach (var stockpile in _stockpiles)
            {
                // Check if stockpile is full
                if (stockpile.GetDepositTile() == null) continue;                    
                
                if (bestStockpile != null)
                {
                    if (Point.Distance(position, stockpile.Location) <
                        Point.Distance(position, bestStockpile.Location))
                    {
                        bestStockpile = stockpile;
                    }
                }
                else
                {
                    bestStockpile = stockpile;
                }
            }
            return bestStockpile;
        }
        return null;
    }

    public Stockpile RequestStockpileWith(int id)
    {
        foreach (var stockpile in _stockpiles)
        {
            if (stockpile.HasGridObject(id))
            {
                return stockpile;
            }
        }
        return null;
    }
    
    // Return specific part based on id
    public Part GetPart(int id)
    {
        return _parts.FirstOrDefault(part => part.ID == id);
    }

    // Return specifc stockpile based on id
    public Stockpile GetStockpile(int id)
    {
        foreach (var stockpile in _stockpiles)
        {
            if (stockpile.HasTile(id)) return stockpile;
        }
        return null;
    }
    
    // Return specific machine based on id
    public Machine GetMachine(int id)
    {
        return _manufacturerManager.GetMachine(id);
    }
    
    // Return specific workshop based on id
    public Workshop GetWorkshop(int id)
    {
        return _manufacturerManager.GetWorkshop(id);
    }
    
    // Setters
    
    public void AddPart(Part part)
    {
        _parts.Add(part);
    }

    public void RemovePart(Part part)
    {
        _parts.Remove(part);
    }

    public void AddStockpile(Stockpile stockpile)
    {
        _stockpiles.Add(stockpile);
    }

    public void RemoveStockpile(Stockpile stockpile)
    {
        _stockpiles.Remove(stockpile);
    }
	
    // Methods
    
}
