﻿using System;
using System.Collections;
using System.Collections.Generic;
using HighlightingSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
	private MachineBuilder _builder;
	private ManufacturerManager _manager;
	private GridObjectManager _gridObjectManager;

	private ManufacturerType _manufacturerType;

	public GameObject Canvas_Floor;
	
	public GameObject InfoPanel,MachinePanel,WorkshopPanel,LowBar,Timer,ItemPanel;

	public LayerMask UImask;
	
	private InfoPanel _infoPanel;
	private MachinePanel _machinePanel;
	private WorkshopPanel _workshopPanel;
	private ItemPanel _itemPanel;
	
	private Ray _ray;
	private RaycastHit _hit;

	private GameObject _selectedGameObject,_preSelectedGameObject;

	void Start ()
	{
		_builder = GetComponent<MachineBuilder>();
		_manager = GetComponent<ManufacturerManager>();
		_gridObjectManager = GetComponent<GridObjectManager>();
		_infoPanel = InfoPanel.GetComponent<InfoPanel>();
		_machinePanel = MachinePanel.GetComponent<MachinePanel>();
		_workshopPanel = WorkshopPanel.GetComponent<WorkshopPanel>();
		_itemPanel = ItemPanel.GetComponent<ItemPanel>();
		InfoPanel.SetActive(false);
	}
	
	private void Update()
	{
		if (Camera.main == null || _builder.IsInBuildMode()) return;

		_ray = Camera.main.ScreenPointToRay(Input.mousePosition); // raycast to game floor grid
		
		// Make a raycast to check for items/machines on the ground, using mouse position.
		if (Physics.Raycast(_ray, out _hit, 100, UImask))
		{
			// For now, only activate when a resouce is hit
			if ((_hit.collider.CompareTag("Resource") || _hit.collider.CompareTag("Part") ))
			{
				// Check if this machine wasn't selected before, disable previous machine pre highlight
				if (_preSelectedGameObject != null && _preSelectedGameObject != _hit.collider.gameObject)
				{
					DisablePreSelectedHighlight();
				}
				
				// Try enable the highlight effect for this new machine selected
				if (_hit.collider.gameObject != _selectedGameObject &&_hit.collider.gameObject.GetComponent<Highlighter>() == null)
				{
					EnablePreSelectedHighlight(_hit.collider.gameObject);
				}
				
				// Load machine data and show basic menu
				if (_itemPanel.GetNextItem()?.ID != _hit.collider.gameObject.GetInstanceID() && _itemPanel.GetCurrentItem()?.ID != _hit.collider.gameObject.GetInstanceID())
				{
					// Grab data since this is a new item
					Part part = _gridObjectManager.GetPart(_hit.collider.gameObject.GetInstanceID());
					// Give the data to HoverItemPanel
					_itemPanel.LoadBasicData(part,part.GetPartData(), part.GetPartAmount());
					//
					_itemPanel.ShowBasicMenu();
				}
				
				// Check if the player clicks, change it to selected
				if (Input.GetButtonDown("Fire1") && _selectedGameObject != _hit.transform.gameObject)
				{
					// Remove selected highlight effect from previous clicked machine
					if (_selectedGameObject != null)
					{
						DisableSelectedHighlight();
					}	
					// Instead of deleting and assigning a highlight component again, just change colors
					ExchangeSelectors();
					// Disable basic panel
					_itemPanel.DisableBasicPanel();
					// Check if advanced machine panel is open and disable it
					if (_machinePanel.IsSelected)
					{
						_machinePanel.DisableAdvancedPanel();
					}
					// Grab data since this is a new item
					Part part = _gridObjectManager.GetPart(_hit.collider.gameObject.GetInstanceID());
					// Give the data to ItemPanel
					_itemPanel.LoadAdvancedData(part,part.GetPartData(), part.GetQualityLevel(), part.GetPartAmount());
					// Show advanced menu since its selected
					_itemPanel.ShowAdvancedMenu(_hit.collider.transform.position);
				}
			}
			else if (_hit.collider.CompareTag("Machine"))
			{
				// Check if this object wasn't selected before, disable previous object pre highlight
				if (_preSelectedGameObject != null && _preSelectedGameObject != _hit.collider.gameObject)
				{
					DisablePreSelectedHighlight();
				}
				
				// Try enable the highlight effect for this new object selected
				if (_hit.collider.gameObject != _selectedGameObject &&_hit.collider.gameObject.GetComponent<Highlighter>() == null)
				{
					EnablePreSelectedHighlight(_hit.collider.gameObject);
				}
				
				// Load machine data and show basic menu
				if (_machinePanel.GetNextMachine()?.ID != _hit.collider.gameObject.GetInstanceID() && _machinePanel.GetCurrentMachine()?.ID != _hit.collider.gameObject.GetInstanceID())
				{
					// Grab data since this is a new item
					Machine machine = _gridObjectManager.GetMachine(_hit.collider.gameObject.GetInstanceID());
					// Give the data to HoverItemPanel
					_machinePanel.LoadBasicData(machine);
					//
					_machinePanel.ShowBasicMenu();
				}

				// Check if the player clicks, change it to selected
				if (Input.GetButtonDown("Fire1") && _selectedGameObject != _hit.transform.gameObject)
				{
					// Remove selected highlight effect from previous clicked object
					if (_selectedGameObject != null)
					{
						DisableSelectedHighlight();
					}				
					// Instead of deleting and assigning a highlight component again, just change colors
					ExchangeSelectors();
					// Disable basic panel
					_machinePanel.DisableBasicPanel();
					_machinePanel.DisableAdvancedPanel();
					_workshopPanel.DisablePanel();
					// Check if advanced item panel is open and disable it
					if (_itemPanel.IsSelected)
					{
						_itemPanel.DisableAdvancedPanel();
					}
					// Grab data since this is a new machine
					Machine machine = _gridObjectManager.GetMachine(_hit.collider.gameObject.GetInstanceID());
					// Give the data to MachinePanel
					_machinePanel.LoadAdvancedData(machine);
					// Show advanced menu since its selected
					_machinePanel.ShowAdvancedMenu(_hit.collider.transform.position);
				}
			}
			else if (_hit.collider.CompareTag("Workshop"))
			{
				// Check if this object wasn't selected before, disable previous object pre highlight
				if (_preSelectedGameObject != null && _preSelectedGameObject != _hit.collider.gameObject)
				{
					DisablePreSelectedHighlight();
				}
				
				// Try enable the highlight effect for this new object selected
				if (_hit.collider.gameObject != _selectedGameObject &&_hit.collider.gameObject.GetComponent<Highlighter>() == null)
				{
					EnablePreSelectedHighlight(_hit.collider.gameObject);
				}
				
				// Check if the player clicks, change it to selected
				if (Input.GetButtonDown("Fire1") && _selectedGameObject != _hit.transform.gameObject)
				{
					// Remove selected highlight effect from previous clicked object
					if (_selectedGameObject != null)
					{
						DisableSelectedHighlight();
					}				
					// Instead of deleting and assigning a highlight component again, just change colors
					ExchangeSelectors();
					// Disable old panel if open
					_workshopPanel.DisablePanel();
					_machinePanel.DisableAdvancedPanel();
					// Check if advanced item panel is open and disable it
					if (_itemPanel.IsSelected)
					{
						_itemPanel.DisableAdvancedPanel();
					}
					// Grab data since this is a new machine
					Workshop workshop = _gridObjectManager.GetWorkshop(_hit.collider.gameObject.GetInstanceID());
					// Give the data to MachinePanel
					_workshopPanel.LoadData(workshop);
					// Show advanced menu since its selected
					_workshopPanel.ShowPanel(_hit.collider.transform.position);
				}
			}
			
			// If empty space or something else is hovered, disable menus
			else
			{
				if (_preSelectedGameObject != null)
				{
					DisablePreSelectedHighlight();			
				}

				if (_itemPanel.IsHovering)
				{
					_itemPanel.DisableBasicPanel();
				}
				if (_machinePanel.IsHovering)
				{
					_machinePanel.DisableBasicPanel();
				}
			}
		}
	}

	public void CloseAdvancedMachinePanel()
	{
		_machinePanel.DisableAdvancedPanel();
		DisableSelectedHighlight();
	}
	
	public void CloseAdvancedItemPanel()
	{
		_itemPanel.DisableAdvancedPanel();
		DisableSelectedHighlight();
	}
	
	public void CloseWorkshopPanel()
	{
		_workshopPanel.DisablePanel();
		DisableSelectedHighlight();
	}

	// Swaps preSelected and Selected vars, changes colors
	private void ExchangeSelectors()
	{
		if (_preSelectedGameObject == null) return;
		Highlighter highlight = _preSelectedGameObject.GetComponent<Highlighter>();

		if (highlight == null) return;
		highlight.ConstantOn(Color.blue);
		_selectedGameObject = _preSelectedGameObject;
		_preSelectedGameObject = null;
	}
	
	private void EnableSelectedHighlight(GameObject go)
	{
		// Add the highlight component
		Highlighter _highlight = go.AddComponent(typeof(Highlighter)) as Highlighter;
		_highlight.ConstantOn(Color.blue);
		_highlight.OccluderOn();

		// This is now the selected item because it has a highlight component
		_selectedGameObject = go;
	}
	
	private void EnablePreSelectedHighlight(GameObject go)
	{
		// Add the highlight component
		Highlighter _highlight = go.AddComponent(typeof(Highlighter)) as Highlighter;
		_highlight.ConstantOn(Color.cyan);
		_highlight.OccluderOn();
					
		// This is now the selected item because it has a highlight component
		_preSelectedGameObject = go;
	}

	private void DisableSelectedHighlight()
	{
		// Destroy highlight component from previous selected object
		if (_selectedGameObject.GetComponent(typeof(Highlighter)))
		{
			Destroy(_selectedGameObject.GetComponent(typeof(Highlighter)));
		}
		// Make selectedGameObject null again so it can be assigned the new highlighted GO
		_selectedGameObject = null;
	}
	
	private void DisablePreSelectedHighlight()
	{
		// Destroy highlight component from previous selected object
		if (_preSelectedGameObject.GetComponent(typeof(Highlighter)))
		{
			Destroy(_preSelectedGameObject.GetComponent(typeof(Highlighter)));
		}
		// Make selectedGameObject null again so it can be assigned the new highlighted GO
		_preSelectedGameObject = null;
	}

	public void ShowInfoPanel()
	{
		InfoPanel.SetActive(true);
		if (_builder.GetCurrentMachine() != null)
		{
			Machine currentMachine = _builder.GetCurrentMachine();
			_infoPanel.SetTitle(currentMachine.GetMachineData().GetName().ToString());
			_infoPanel.SetDescriptionTitle(currentMachine.GetMachineData().GetMachineType().ToString());
			_infoPanel.SetDescriptionText(currentMachine.GetMachineData().GetDescription());
			_infoPanel.SetPrice(currentMachine.GetMachineData().GetPrice());
			_infoPanel.SetHours(currentMachine.GetMachineData().GetHours());
		} else if (_builder.GetCurrentPart() != null)
		{
			Part currentPart = _builder.GetCurrentPart();
			_infoPanel.SetTitle(currentPart.GetPartData().GetName().ToString());
			_infoPanel.SetDescriptionTitle(currentPart.GetPartData().GetPartType().ToString());
			_infoPanel.SetDescriptionText(currentPart.GetPartData().GetDescription());
			/*
			_infoPanel.SetPrice(currentPart.GetPartData().GetValue());
			_infoPanel.SetHours(currentPart.GetPartData().GetQuality());
			*/
		}
	}
	
	public GameObject GetCanvasFloor()
	{
		return Canvas_Floor;
	}	
}