﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Pathfinding;
using UnityEngine;

public class WorkManager : MonoBehaviour
{
	private List<Worker> _workers;
	
	void Start () {
		
		_workers = new List<Worker>();		
	}
	
	void Update () {

		UpdateWorkforce();
	}

	void UpdateWorkforce()
	{
		foreach (var worker in _workers)
		{	
			if (worker.IsOnPath() && !worker.IsMoving())
			{
				worker.SetMoving(true);
				worker.GetBody().SetActive(false);
				AIPath mover = worker.GetBody().AddComponent(typeof(AIPath)) as AIPath;
				/*
				RaycastModifier raycastModifier = worker.GetBody().AddComponent(typeof(RaycastModifier)) as RaycastModifier;
				raycastModifier.thickRaycast = true;
				raycastModifier.thickRaycastRadius = 0.5f;
				raycastModifier.raycastOffset = new Vector3(0,0.5f,0);
				*/
				mover.setWorker(worker);
				mover.targetPosition = worker.GetTarget();
				worker.GetBody().SetActive(true);
			}
		}
	}
		
	// Return avalible worker
	public Worker RequestWorker()
	{
		foreach (var worker in _workers)
		{
			if (worker.GetWorkerState() == WorkerState.Idle)
			{
				return worker;
			}
		}
		return null;
	}
	
	// Return closest avalible worker
	public Worker RequestClosestWorker(Point destination)
	{
		Worker bestWorker = null;
		
		foreach (var worker in _workers)
		{
			if (worker.GetWorkerState() == WorkerState.Idle && !worker.IsOperator())
			{
				if (bestWorker != null)
				{
					if (Point.Distance(destination,worker.Location) < Point.Distance(destination,bestWorker.Location))
					{
						bestWorker = worker;
					}
				}
				else
				{
					bestWorker = worker;
				}
			}
		}
		
		return bestWorker;
	}
	
	// Return specific worker
	public Worker GetWorker(int id)
	{
		return _workers.FirstOrDefault(worker => worker.ID == id);
	}
	
	// Setters

	public void AddWorker(Worker worker)
	{
		_workers.Add(worker);
	}
	
	public void RemoveWorker(Worker worker)
	{
		_workers.Remove(worker);
	}
	
}
