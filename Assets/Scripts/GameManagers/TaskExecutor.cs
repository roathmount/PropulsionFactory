﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class TaskExecutor : MonoBehaviour
{
	private GridManager _gridManager;
	private GridObjectManager _gridObjectManager;

	private List<Task> _currentTasks;

	private void Start()
	{
		_gridManager = GetComponent<GridManager>();
		_gridObjectManager = GetComponent<GridObjectManager>();

		_currentTasks = new List<Task>();
	}

	private void Update()
	{
		// Look into stored tasks
		foreach (var task in _currentTasks.ToList())
		{
			// If task is finished, remove. If not, proceed with actions
			if (!task.IsTaskFinished() && task.GetCurrentAction() != null )
			{
				TaskAction currentAction = task.GetCurrentAction();
				
				// Check if the current action is finished
				if (currentAction.IsActionFinished())
				{
					task.UseNextAction();
					return;
				}
				
				Worker currentWorker = task.GetDesignatedWorker();
				
				// TODO: move this to separate methods lol (clusterfuck)
				if (currentAction.GetActionType() == ActionType.Deposit)
				{
					if (currentWorker.Location == currentAction.GetDestination())
					{
						// TODO: (task system) change what to do depending on deposited gridObject
						switch (currentAction.GetGridObjectType())
						{
							// Desposited a resource
							case GridObjectType.Part:
								OnDepositPart(task, currentAction, currentWorker);
								break;
							case GridObjectType.Resource:
								OnDepositPart(task, currentAction, currentWorker);
								break;
							case GridObjectType.Machine:
								break;
							case GridObjectType.Stockpile:
								break;
							case GridObjectType.Workshop:
								break;
							case GridObjectType.Product:
								break;
							case GridObjectType.Worker:
								break;
							case GridObjectType.Default:
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
				}
				else if (currentAction.GetActionType() == ActionType.Gather)
				{
					if (currentWorker.Location == currentAction.GetDestination())
					{
						// TODO: (task system) change what to do depending on gathered gridObject
						switch (currentAction.GetGridObjectType())
						{
							case GridObjectType.Part: // Gather part from ground
								OnGatheredFromGround(currentAction, currentWorker);
								break;
							case GridObjectType.Resource: // Gather resource from ground
								OnGatheredFromGround(currentAction, currentWorker);
								break;
							case GridObjectType.Machine: // Gather part from machine
								OnGatheredFromMachine(currentAction, currentWorker);
								break;
							case GridObjectType.Workshop: // Gather part from workshop
								OnGatheredFromWorkshop(currentAction, currentWorker);
								break;
							case GridObjectType.Stockpile:
								break;
							case GridObjectType.Product:
								break;
							case GridObjectType.Worker:
								break;
							case GridObjectType.Default:
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
					}
					else
					{
						// TODO: (task system) add new action to go for a new gridObject	
					}
				}
				else if (currentAction.GetActionType() == ActionType.Move)
				{
					if (!currentWorker.IsOnPath() && currentWorker.GetWorkerState() == WorkerState.OnTask)
					{
						// TODO: (pathfinding) remplace with new pathfinding methods

						GridTile dest = _gridManager.GetTile(currentAction.GetDestination().X, currentAction.GetDestination().Y);

						currentWorker.SetPath(new Vector3(dest.X + 0.5f, 0, dest.Y + 0.5f));
					}

					if (currentWorker.GetWorkerState() == WorkerState.Done)
					{
						currentWorker.SetWorkerState(WorkerState.OnTask);
						currentAction.SetActionFinished();
					}

					// Look to move into that position
				} else if (currentAction.GetActionType() == ActionType.Operate)
				{
					if (currentWorker.Location == currentAction.GetDestination())
					{
						if (currentAction.GetGridObjectType() == GridObjectType.Machine) // Operating machine
						{
							OnOperatingMachine(currentAction,currentWorker);
						} 
						else if (currentAction.GetGridObjectType() == GridObjectType.Workshop) // Operating workshop
						{
							OnOperatingWorkshop(currentAction,currentWorker);
						}
					}
				}
			}
			else
			{
				// If the worker finished the task doing anything but operating a machine, put it idle so it can be avaliable for other tasks
				if (task.GetDesignatedWorker().GetWorkerState() != WorkerState.Operating)
				{
					task.GetDesignatedWorker().SetWorkerState(WorkerState.Idle);
				}
				FinishTask(task);
			}
		}
	}

	private void OnDepositPart(Task task, TaskAction currentAction, Worker currentWorker)
	{
		// Get the part using worker stored gridObject ID
		Part refereceResource = _gridObjectManager.GetPart(currentWorker.GetCarriedGridObjectID());
	
		if (task.GetTaskType() == TaskType.RechargeManufacturer)
		{
			refereceResource.Location = currentAction.GetDestination();
			// Remove it from worker
			currentWorker.SetCarriedGridObjectID(0);
			// Destroy resource gameObject
			// TODO: (general gameplay) Add resource gameobject near machine to know resource amount on machine
			
		
			// Add resource to manufacturer
			Manufacturer manufacturer = _gridObjectManager.GetMachine(currentAction.GetGridObjectID()) ?? (Manufacturer) _gridObjectManager.GetWorkshop(currentAction.GetGridObjectID());
	
			if (manufacturer.GetResource(refereceResource.GetPartData().GetPartType()) != null)
			{
				int amount = refereceResource.UsePart(currentAction.GetActionAmount());
				manufacturer.AddResource(refereceResource.GetPartData().GetPartType(),amount);
				refereceResource.DestroyGameObject();
			}
			else
			{
				refereceResource.SetQueued(false);
				manufacturer.AddResource(refereceResource);
			}
		}
		else if (task.GetTaskType() == TaskType.TransportGridObject)
		{
			GameObject carriedGameObject = currentWorker.GetBody().transform.GetChild(0).gameObject;

			if (currentAction.GetGridObjectID() != 0) // Storing on stockpile
			{
				Stockpile sp = _gridObjectManager.GetStockpile(currentAction.GetGridObjectID());
				refereceResource.Location = currentAction.GetDestination();
				
				bool canStore = sp.Store(refereceResource,refereceResource.GetPartData().GetSize());
				if (canStore)
				{
					refereceResource.SetQueued(false);
					// TODO: (task system & general gameplay) Add parts to manufacturing process
					// Remove resource ID from worker
					currentWorker.SetCarriedGridObjectID(0);
					// TODO: replace destroying from worker list TO carried part ID object	
					carriedGameObject.transform.position = new Vector3(currentAction.GetDestination().X + refereceResource.GetPartData().GetSize().x/2, 0,
						currentAction.GetDestination().Y + refereceResource.GetPartData().GetSize().y/2);
					carriedGameObject.transform.rotation = Quaternion.identity;
					carriedGameObject.transform.parent = sp.GetBody().transform;
				}
				else
				{
					GridTile newTile = sp.GetDepositTile(refereceResource.Location);
					if (newTile != null)
					{
						FinishTask(task);
						TransportGridObject replacementTask = new TransportGridObject(TaskType.TransportGridObject,TaskPriority.Urgent,newTile);
						replacementTask.SetDesignatedWorker(task.GetDesignatedWorker());
						ExecuteTask(replacementTask);
					}
					else
					{
						Stockpile newSp = _gridObjectManager.RequeStockpileOf(refereceResource.Location);
						FinishTask(task);
						TransportGridObject replacementTask = new TransportGridObject(TaskType.TransportGridObject,TaskPriority.Urgent,newSp.GetDepositTile(refereceResource.Location));
						replacementTask.SetDesignatedWorker(task.GetDesignatedWorker());
						ExecuteTask(replacementTask);
					}
				}
			}
		}
		currentAction.SetActionFinished();
	}


	private void OnGatheredFromMachine(TaskAction currentAction, Worker currentWorker)
	{
		// Get machine
		Machine machine = _gridObjectManager.GetMachine(currentAction.GetGridObjectID());
		// Remove gridObject from machine
		// TODO: store part on gridObject manager: part list
		Part part = machine.WithdrawPart();
		part.Location = new Point(0,0);
		// Set part gameobject for worker to carry
		part.GetBody().transform.parent = currentWorker.GetBody().transform;
		part.GetBody().transform.localPosition = new Vector3(0,0,1);
		part.GetBody().transform.rotation = Quaternion.identity;
		// Save part on list
		_gridObjectManager.AddPart(part);
									
		// Only store part ID into worker
		currentWorker.SetCarriedGridObjectID(part.ID);
		// End gathering part from machine
		currentAction.SetActionFinished();
	}

	private void OnGatheredFromWorkshop(TaskAction currentAction, Worker currentWorker)
	{
		// Get workshop
		Workshop workshop = _gridObjectManager.GetWorkshop(currentAction.GetGridObjectID());
		// Remove gridObject from machine
		// TODO: store part on gridObject manager: part list
		Part productPart = workshop.WithdrawPart();
		productPart.Location = new Point(0,0);
		// Set part gameobject for worker to carry
		productPart.GetBody().transform.parent = currentWorker.GetBody().transform;
		productPart.GetBody().transform.localScale = new Vector3(1,1,1);
		productPart.GetBody().transform.localPosition = new Vector3(0,0,1);
		productPart.GetBody().transform.rotation = Quaternion.identity;
		// Save part on list
		_gridObjectManager.AddPart(productPart);
									
		// Only store part ID into worker
		currentWorker.SetCarriedGridObjectID(productPart.ID);
		// End gathering part from machine
		currentAction.SetActionFinished();
	}

	private void OnGatheredFromGround(TaskAction currentAction, Worker currentWorker)
	{
		// get the actual gathered part as reference
		Part referencePart = _gridObjectManager.GetPart(currentAction.GetGridObjectID());

		if (referencePart != null)
		{

			// Generate a new gameobject for visualization of carried resource
			GameObject visualPart =
				GameObject.Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(referencePart.GetPartData().GetPartType().ToString()),
					currentWorker.GetBody().transform.position, Quaternion.identity,
					currentWorker.GetBody().transform) as GameObject;
			// Define a new temporal part for the worker to carry
			// TODO: (task system & general gameplay) replace default gathered value from resource based on weight/worker propierties
			Part carriedPart = new Part(referencePart.GetPartData(), 0, 0,
				referencePart.UseReserve(currentAction.GetActionAmount()), visualPart);
			// Check if all resources from that tile are being gathered and if thats the case, withdraw and unreserve the space from stockpile
			// TODO remove debug check for aluminium and niobium as they are debug spawned
			if (referencePart.GetPartAmount() == 0 && referencePart.GetReservedAmount() <= 0 && referencePart.GetPartData().GetPartType() != PartType.Aluminium &&
			    referencePart.GetPartData().GetPartType() != PartType.Niobium)
			{
				Stockpile sp = _gridObjectManager.RequestStockpileWith(referencePart.ID);
				//Debug.Log(referencePart.ID + " "+ referencePart.GetPartData().GetPartType()+"     pos: "+referencePart.Location.X+" "+referencePart.Location.Y);
				sp.Withdraw(referencePart,referencePart.GetPartData().GetSize());
			}

			// Place carried resource at prefered worker position
			visualPart.transform.localPosition = new Vector3(0, 0, 1);
			visualPart.transform.rotation = currentWorker.GetBody().transform.rotation;
			// Save resource onto resource list for later extraction
			_gridObjectManager.AddPart(carriedPart);

			// Only store resource ID into worker
			currentWorker.SetCarriedGridObjectID(carriedPart.ID);
			// End gathering action
		}
		else // No resource to gather, new task for new resource
		{
			//Part resource = _gridObjectManager.RequestClosestPart(item.GetItemType(), manufacturer.Location);
			Debug.Log(referencePart.GetPartData().GetPartType());
		}

		currentAction.SetActionFinished();
	}

	private void OnOperatingMachine(TaskAction currentAction, Worker currentWorker)
	{
		// Get machine

		Machine machine = _gridObjectManager.GetMachine(currentAction.GetGridObjectID());

		if (machine.GetCurrentOperator() != null && !machine.IsOperated())
		{
			currentWorker.SetWorkerState(WorkerState.Operating);
			machine.SetOperated(true);
			machine.SetManufacturerState(ManufacturerState.Working);
		}
		
		currentAction.SetActionFinished();
	}
	
	private void OnOperatingWorkshop(TaskAction currentAction, Worker currentWorker)
	{
		currentAction.SetActionFinished();
	}
	
	// New task from task manager added to current list
	public void ExecuteTask(Task task)
	{
		_currentTasks.Add(task);
	}

	private void FinishTask(Task task)
	{
		_currentTasks.Remove(task);
	}
	
}
