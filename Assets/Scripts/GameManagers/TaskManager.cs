﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TaskManager : MonoBehaviour
{
	private TaskExecutor _executor;
	private WorkManager _workManager;
	private GridObjectManager _gridObjectManager;

	private List<Task> _taskQueue;
	
	void Start ()
	{

		_executor = GetComponent<TaskExecutor>();
		_workManager = GetComponent<WorkManager>();
		_gridObjectManager = GetComponent<GridObjectManager>();
			
		_taskQueue = new List<Task>();
	}
	
	void Update()
	{

		if (_taskQueue.Count > 0)
		{

			Task currentTask = LookForTask(TaskPriority.Urgent);

			if (currentTask != null)
			{
				TryStartTask(currentTask);
			}
			else
			{
				currentTask = LookForTask(TaskPriority.High);

				if (currentTask != null)
				{
					TryStartTask(currentTask);
				}
				else
				{
					currentTask = LookForTask(TaskPriority.Normal);

					if (currentTask != null)
					{
						TryStartTask(currentTask);
					}
					else
					{
						currentTask = LookForTask(TaskPriority.Low);

						if (currentTask != null)
						{
							TryStartTask(currentTask);
						}
						else
						{
							Debug.LogError("Task has no defined priority");
						}
					}
				}
			}
		}
	}

	private Task LookForTask(TaskPriority priority)
	{
		return _taskQueue.Count > 0 ? _taskQueue.FirstOrDefault(task => task.GetPriority() == priority) : null;
	}

	private void TryStartTask(Task task)
	{
		if (task.GetDesignatedWorker() == null)
		{

			Worker avaliableWorker = _workManager.RequestClosestWorker(task.GetCurrentAction().GetDestination());

			if (avaliableWorker != null)
			{
				task.SetDesignatedWorker(avaliableWorker);
				avaliableWorker.SetWorkerState(WorkerState.OnTask);
				avaliableWorker = null;

				StartTask(task);
			}
		}
		else
		{
			StartTask(task);
		}
	}

	public void StartTask(Task task)
	{
		_taskQueue.Remove(task);
		_executor.ExecuteTask(task);
	}

	public void GenerateTask(TaskType type, Manufacturer manufacturer, RecipeItem item, int amount)
	{
		if (type == TaskType.RechargeManufacturer)
		{
			// Look for the resource needed
			Part resource = _gridObjectManager.RequestClosestPart(item.GetItemType(), amount, manufacturer.Location);
			if (resource != null && resource.GetPartAmount() > 0)
			{
				int unreservedAmount = resource.ReserveAmount(amount);			
				manufacturer.GetResource(item.GetItemType()).SetQueued(true);
				RechargeManufacturer task = new RechargeManufacturer(type, TaskPriority.Normal, manufacturer, resource, amount - unreservedAmount);
				if (unreservedAmount != 0)
				{
					GenerateTask(TaskType.RechargeManufacturer, manufacturer, item, unreservedAmount);
				}
				manufacturer.SetManufacturerState(ManufacturerState.Idle);

				if (manufacturer.GetCurrentOperator() != null && manufacturer.IsOperated())
				{
					manufacturer.SetOperated(false);
					manufacturer.GetCurrentOperator().SetWorkerState(WorkerState.OnTask);
					task.SetDesignatedWorker(manufacturer.GetCurrentOperator());
				}
				_taskQueue.Add(task);
			}
			else
			{
				// TODO: remplace working state to waiting state for resource/part to arrive.
				manufacturer.SetManufacturerState(ManufacturerState.Working);
			}
		}
	}
	
	// TODO: (task manager) remplace assigned gridObjects for generic ID integration
	public void GenerateTask(TaskType type, Manufacturer manufacturer)
	{
		if (type == TaskType.RechargeManufacturer)
		{

		}
		else if (type == TaskType.OperateManufacturer)
		{
			// Check if the manufacturer is being operated to prevent task
			if (!manufacturer.IsOperated())
			{
				
				OperateManufacturer task = new OperateManufacturer(TaskType.OperateManufacturer, TaskPriority.Normal, manufacturer);

				if (manufacturer.GetCurrentOperator() != null)
				{
					manufacturer.SetOperated(false);
					task.SetDesignatedWorker(manufacturer.GetCurrentOperator());
					manufacturer.GetCurrentOperator().SetWorkerState(WorkerState.OnTask);
					_taskQueue.Add(task);
				}
				else
				{
					Worker avaliableWorker = _workManager.RequestClosestWorker(task.GetCurrentAction().GetDestination());

					if (avaliableWorker != null)
					{
						task.SetDesignatedWorker(avaliableWorker);
						manufacturer.SetCurrentOperator(avaliableWorker);
						avaliableWorker.SetWorkerState(WorkerState.OnTask);
						avaliableWorker.SetOperator(true);
						avaliableWorker = null;
						_taskQueue.Add(task);
					}
					else
					{
						manufacturer.SetManufacturerState(ManufacturerState.Working);
					}	
				}
			}
			else
			{
				manufacturer.SetManufacturerState(ManufacturerState.Working);
			}
		}
		else if (type == TaskType.TransportGridObject)
		{
			// Check if the transport task is for removing stored pard
			if (manufacturer.GetStoredPart() != null && manufacturer.GetStoredPart().IsFinished())
			{
				// TODO: (stockpile) sort to stockpile with part properties
				TransportGridObject task;

				Stockpile sp = _gridObjectManager.RequeStockpileOf(manufacturer.Location);
				if (sp != null)
				{
					GridTile[] tiles = sp.GetDepositTiles(manufacturer.GetStoredPart().GetPartData().GetSize(),manufacturer.Location);
					if (tiles[0] != null)
					{
						for (int i = 0; i < tiles.Length; i++)
						{
							sp.ReserveTile(tiles[i]);
						}
						manufacturer.GetStoredPart().SetQueued(true);
						task = new TransportGridObject(type, TaskPriority.Normal, manufacturer, tiles[0]);
						manufacturer.SetManufacturerState(ManufacturerState.Idle);
						
						if (manufacturer.GetCurrentOperator() != null && manufacturer.IsOperated())
						{
							manufacturer.SetOperated(false);
							manufacturer.GetCurrentOperator().SetWorkerState(WorkerState.OnTask);
							task.SetDesignatedWorker(manufacturer.GetCurrentOperator());
						}
						
						_taskQueue.Add(task);
					}
					else
					{
						// TODO: (stockpile) make stored part stay there while a stockpile is free
						manufacturer.SetManufacturerState(ManufacturerState.Working);
					}
				}
				else
				{
					// TODO: (stockpile) make stored part stay there while a stockpile is free
					manufacturer.SetManufacturerState(ManufacturerState.Working);					
				}
			}
		}
	}
}
