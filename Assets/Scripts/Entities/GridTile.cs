﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GridTile : GridObject
{
    public bool Passable,Service,Reserved;
    private int _currentGridObject;
    public int ID;
 
    // tiles ID != objects ID
    public GridTile(int id,int x, int y)
        : base(0, x, y, GridObjectType.Default)
    {
        ID = id;
        Passable = true;
        Service = false;
        Reserved = false;
    }

    public int GetGridObjectID()
    {
        return base.ID;
    }

    public int GetCurrentGridObject()
    {
        return _currentGridObject;
    }
    
    public void SetCurrentGridObject(int gridObjectID)
    {
        _currentGridObject = gridObjectID;
    }
    
    public void SetGridObjectID(int id)
    {
        base.ID = id;
    }
}