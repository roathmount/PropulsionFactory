﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;
using UnityEngine;

public class Worker : GridObject
{

    private bool _onPath,_moving,_operator;

    private WorkerState _currentWorkerState;
    
    private Vector3 _pathTarget;

    private readonly GameObject _gameObject;

    private int _carriedGridObjectID;
    
    public Worker(int x, int y, GameObject body) : base (body.GetInstanceID(), x, y, GridObjectType.Worker)
    {
        _gameObject = body;
        _gameObject.transform.position = new Vector3(x+0.5f,0,y+0.5f);
        
        _currentWorkerState = WorkerState.Idle;
        
        _moving = false;
        _operator = false;
    }
    
    // Getters
    
    public WorkerState GetWorkerState()
    {
        return _currentWorkerState;
    }
    
    public bool IsOnPath()
    {
        return _onPath;
    }
    
    public bool IsMoving()
    {
        return _moving;
    }

    public bool IsOperator()
    {
        return _operator;
    }
    
    public GameObject GetBody()
    {
        return _gameObject;
    }
    
    public Vector3 GetTarget()
    {
        return _pathTarget;
    }

    public int GetCarriedGridObjectID()
    {
        return _carriedGridObjectID;
    }
    
    // Setters

    public void SetWorkerState(WorkerState state)
    {
        _currentWorkerState = state;
    }
    
    public void SetMoving(bool value)
    {
        _moving = value;
    }
    
    public void SetOperator(bool value)
    {
        _operator = value;
    }

    public void SetCarriedGridObjectID(int id)
    {
        _carriedGridObjectID = id;
    }
        
    // gets new path
    public void SetPath(Vector3 target)
    {
        _pathTarget = target;
        _onPath = true;
    }
    
    // Methods

    public void OnObjetiveReached()
    {
        _onPath = false;
        _moving = false;
        Object.Destroy(_gameObject.GetComponent<AIPath>());
        //Object.Destroy(_gameObject.GetComponent<RaycastModifier>());
        Object.Destroy(_gameObject.GetComponent<Seeker>());

        _currentWorkerState = WorkerState.Done;
    }
    
}
