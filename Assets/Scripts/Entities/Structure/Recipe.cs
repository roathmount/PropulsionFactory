﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RecipeList
{
    public List<Recipe> Recipes;

    public RecipeList()
    {
        Recipes = new List<Recipe>();
    }

    public Recipe GetPartRecipe(PartType type)
    {
        foreach (var recipe in Recipes)
        {
            if (recipe.GetPartType() == type)
            {
                return recipe;
            }
        }
        return new Recipe(PartType.None);
    }
}

[Serializable]
public class RecipeItemList
{
    public List<RecipeItem> _items;

    public RecipeItemList()
    {
        _items = new List<RecipeItem>();
    }

    public RecipeItem GetRecipeItem(PartType type)
    {
        foreach (var item in _items)
        {
            if (item.GetItemType() == type)
            {
                return item;
            }
        }
        return null;
    }
}

[Serializable]
public class RecipeItem
{
    [SerializeField] private PartType _itemType;
    [SerializeField] private bool _finished;
    [SerializeField] private int _itemAmount;

    public RecipeItem(PartType type, int amount, bool finished)
    {
        _itemType = type;
        _itemAmount = amount;
        _finished = finished;
    }

    // Getters
    
    public int GetItemAmount()
    {
        return _itemAmount;
    }

    public bool RequieresItemFinished()
    {
        return _finished;
    }

    public PartType GetItemType()
    {
        return _itemType;
    }
    
    // Setters

    public int RemoveItemAmount(int amount)
    {
        if ((_itemAmount - amount) > 0)
        {
            _itemAmount -= amount;
            return 0;
        }
        else
        {
            int leftovers = amount - _itemAmount;
            _itemAmount = 0;
            return leftovers;
        }
    }
}

[Serializable]
public class Recipe {

    [SerializeField] private PartType _partType;
    [SerializeField] private RecipeItemList _itemsList;

    public Recipe(PartType partTypeType)
    {
        _partType = partTypeType;
        _itemsList  = new RecipeItemList();
    }

    
    public Recipe(PartType partTypeType, List<RecipeItem> items)
    {
        _partType = partTypeType;
        _itemsList  = new RecipeItemList();
        _itemsList._items = items;
    }

    // Getters
    
    public List<RecipeItem> GetItems()
    {
        return _itemsList._items;
    }
    
    public RecipeItem GetCurrentItem()
    {
        return _itemsList._items[0];
    }

    public RecipeItem GetRecipeItem(PartType partType)
    {
        foreach (var item in _itemsList._items)
        {
            if (item.GetItemType() == partType)
            {
                return item;
            }
        }
        Debug.LogError("error!");
        return new RecipeItem(PartType.None,0,false);
    }

    public PartType GetPartType()
    {
        return _partType;
    }
    
    // Setters

    public void AddItem(RecipeItem item)
    {
        _itemsList._items.Add(item);
    }

    public void RemoveItem(RecipeItem item)
    {
        _itemsList._items.Remove(item);
    }
    
}
