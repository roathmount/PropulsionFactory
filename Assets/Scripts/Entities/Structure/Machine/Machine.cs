﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Object;

public class Machine : Manufacturer
{

    private MachineData _data;
        
    // Constructors

    // Constructors used for building not for final machine placement
    public Machine(MachineData newData, int id, int x, int y) : base(id, x, y, GridObjectType.Machine)
    {
        _data = newData;
    }
    
    public Machine(MachineData newData, int id, int x, int y,GameObject body) : base(id, x, y, GridObjectType.Machine, body)
    {
        _data = newData;
    }

    // Getters
    
    public MachineData GetMachineData()
    {
        return _data;
    }
    
    // Methods

    public override void Work()
    {

        // Check if the machine has a designed operator
        if (_currentOperator == null)
        {
            // Set machine idle while waiting for a worker
            SetManufacturerState(ManufacturerState.Idle);
            // Send request looking for any worker avaliable
            Messenger<Machine>.Broadcast("new ManufacturerRequestOperator",this);
            return;
        }
        // If the machine isnt being operated at the moment, dont work
        if (!Operated)
        {
            if (_currentOperator.IsOperator())
            {            
                // Set machine idle while waiting for a worker
                SetManufacturerState(ManufacturerState.Idle);
                // Send request looking for any worker avaliable
                Messenger<Machine>.Broadcast("new ManufacturerRequestOperator",this);
            }
            return;
        }
        
        // Check if the machine already finished its current product
        if ((_storedPart != null && !Empty))
        {
            // If the stored part isn't queued (reserved by a worker) ask to withdraw it again
            if (!_storedPart.IsQueued())
            {
                Operated = false;
                _currentOperator.SetWorkerState(WorkerState.Idle);
                _currentOperator.SetOperator(false);
                _currentOperator = null;
                // Send request to withdraw part
                Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
            }
            return;
        }
        
        foreach (var item in _currentRecipe.GetItems().ToArray())
        {
            if (item.GetItemAmount() <= 0)
            {
                _currentRecipe.RemoveItem(item);
                continue;
            }
            // Get the resources used in the manufacturing process
            int usedResources = UseResource(item.GetItemType());
            
            // TODO: (recipe system) Also send recharge request when resource amount is lower than 1/4 of resource amount limit
            // No resource amount left for manufacturing, request recharge
            Part resource = GetResource(item.GetItemType());
            
            if (usedResources == 0 && !resource.IsQueued())
            {
                // Set machine idle while waiting for resources
                SetManufacturerState(ManufacturerState.Idle);
                // Request
                Messenger<Manufacturer, RecipeItem>.Broadcast("new ManufacturerRequestRecharge", this, item);
                continue;
            }
            // Remove amount from recipe item using stored resource amount
            int recoveredResources = item.RemoveItemAmount(usedResources);
            AddWork(usedResources + recoveredResources);
            // Check if all resources sent were used
            if (recoveredResources > 0)
            {
                // Add surplus of resources back into the list
                AddResource(item.GetItemType(), recoveredResources);
            }
        }
    }

    
    public override void ManufacturePart(PartData data, Recipe recipe)
    {
        GameObject partGo = Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(data.GetPartType().ToString()),_gameObject.transform) as GameObject;
        _storedPart = new Part(data, 0, 0,partGo);
        partGo.SetActive(false);
        // Set current part recipe
        _currentRecipe = recipe;
        // TODO: (Recipe system) Check for already stored resources to use, request them remove if they dont apply to the current recipe

        WorkAmount = 0;
        
        foreach (var item in _currentRecipe.GetItems())
        {
            WorkAmount += item.GetItemAmount();
        }

        TotalWorkAmount = WorkAmount;
        
        SetManufacturerState(ManufacturerState.Working);
    }

    protected override void FinishManufacture()
    {
        base.FinishManufacture();
        // Machine finished work so it becomes idle
        SetManufacturerState(ManufacturerState.Idle);
        
        Vector3 spawnPoint = new Vector3();
        Quaternion rotation = new Quaternion();
        
        foreach (Transform children in _gameObject.transform)
        {
            if (children.name == "SpawnPoint")
            {
                spawnPoint = children.transform.position;
                rotation = children.transform.rotation;
            }
        }
        _storedPart.GetBody().SetActive(true);
        _storedPart.GetBody().transform.position = spawnPoint;
        _storedPart.GetBody().transform.rotation = rotation;
        _storedPart.SetFinish(Quality.Normal);
        _storedPart.AddAmount(_storedPart.GetPartData().GetAmountLimit());
        Empty = false;
        
        // Being operations to make the machine avaliable again
        Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
    }
    
    public Part WithdrawPart()
    {
        if (!Empty)
        {
            Empty = true;
            var temp = _storedPart;
            _storedPart = null;
            SetManufacturerState(ManufacturerState.Waiting);
            temp.Location = GetServiceTile().Location; // Setting part location at one of the service tiles for worker to go to that tile
            return temp;
        }
        else
        {
            return null;
        }
    }
    
    // Inverts X and Y cords of the machine if its rotated
    public void InvertSize()
    {
        var tempX = _data.GetSize().y;
        var tempY = _data.GetSize().x;

        _data.SetSize((int)tempX,(int)tempY);
    }
    
}
