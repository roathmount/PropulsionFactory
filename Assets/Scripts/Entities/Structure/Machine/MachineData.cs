﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
[Serializable]
public class MachineDataList
{
    public List<MachineData> dataList;

    public MachineDataList()
    {
        dataList = new List<MachineData>();
    }

    public MachineData GetMachine(ManufacturerType type)
    {
        foreach (var data in dataList)
        {
            if (data.GetMachineType() == type)
            {
                return data;
            }
        }
        return dataList[0];
    }
}

[Serializable]
public struct MachineData  {

    [SerializeField]
    private string _name;
    [SerializeField]
    private ManufacturerType _type;
    [SerializeField]
    private Vector2 _size;
    [SerializeField]
    private string _description;
    [SerializeField]
    private int _price;
    [SerializeField]
    private int _hours;
    [SerializeField]
    private int _health;
    [SerializeField]
    private PartType[] _machineParts;

    // Constructors

    public MachineData(string machineName, ManufacturerType machineType, int sizeX, int sizeY, int newHealth,int newPrice, int newHours, PartType[] parts , string desc)
    {
        _name = machineName;
        _type = machineType;
        _size = new Vector2(sizeX,sizeY);
        _health = newHealth;
        _price = newPrice;
        _hours = newHours;
        _machineParts = parts;
        _description = desc;
    }
    
    // Getters
    
    public int GetHealth()
    {
        return _health;
    }
    
    public int GetPrice()
    {
        return _price;
    }

    public int GetHours()
    {
        return _hours;
    }

    public string GetDescription()
    {
        return _description;
    }

    public string GetName()
    {
        return _name;
    }
        
    public Vector2 GetSize()
    {
        return _size;
    }

    public ManufacturerType GetMachineType()
    {
        return _type;
    }

    public PartType[] GetMachineParts()
    {
        return _machineParts;
    }
    
    // Setters

    public void SetName(string newName)
    {
        _name = newName;
    }
    
    public void SetHealth(int amount)
    {
        _health = amount;
    }

    public void SetPrice(int amount)
    {
        _price = amount;
    }
    
    public void SetHours(int amount)
    {
        _hours = amount;
    }

    public void SetDescription(string newDesc)
    {
        _description = newDesc;
    }

    public void SetSize(Vector2 newSize)
    {
        _size = newSize;
    }
    
    public void SetSize(int sizeX,int sizeY)
    {
        _size.Set(sizeX,sizeY);
    }

    public void SetMachineParts(PartType[] array)
    {
        _machineParts = array;
    } 
    
}
