﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MachineBuilder : MonoBehaviour
{

	private Ray _ray;
	private RaycastHit _hit;

	private Vector3 _tilePos, _oldTilePos,startPoint;
	private Vector2 _selectedAreaSize;

	private GameObject _reserverArea;
	private GameObject _machineGhost;
	public GameObject MachineList;

	private GridManager _gridManager;
	
	private GridObjectManager _gridObjectManager;
	private WorkManager _workManager;
	
	private ManufacturerManager _manufacturerManager;
	private UIManager _uiManager;

	private GridObjectType _currentGridObjectType;
	private ManufacturerType _currentManufacturerType;
	
	private Workshop _currentWorkshop;

	private Machine _currentMachine;
	private Part _currentPart;
	private Module _currentModule;
	private MachineDataList _machineDatas;
	
	private WorkshopDataList _workshopDatas;

	private float _offsetX, _offsetY;

	private bool _building;
	private bool _rotated, _overlaping, _deconstructMode, _outside;
	private bool _endPath = false;
	private GridTile _selectedTile,_currentTile;

	private Material _normalTile, _errorTile, _normalGhost, _errorGhost;

	private void Start ()
	{
		_building = false;
		
		_gridManager = GetComponent<GridManager>();
		
		_gridObjectManager = GetComponent<GridObjectManager>();
		_workManager = GetComponent<WorkManager>();
		_manufacturerManager = GetComponent<ManufacturerManager>();
		_uiManager = GetComponent<UIManager>();
		
		startPoint = new Vector3();
		_selectedAreaSize = new Vector2();
		
		_rotated = false;
		_reserverArea = Instantiate(Resources.Load("Prefabs/HighlightTile")) as GameObject;
		_machineGhost = new GameObject();
						
		_machineDatas = new MachineDataList();
		TextAsset machineDataFile = Resources.Load<TextAsset>("Machines");
		_machineDatas = JsonUtility.FromJson<MachineDataList>(machineDataFile.text);
		
		_workshopDatas = new WorkshopDataList();
		TextAsset workshopDataFile = Resources.Load<TextAsset>("Workshops");
		_workshopDatas = JsonUtility.FromJson<WorkshopDataList>(workshopDataFile.text);
		
		Messenger<PartType, int, bool>.AddListener("new WorkshopProductBuild", OnWorkshopProductBuild);
		Messenger<ModuleType, int, bool>.AddListener("new WorkshopModuleBuild", OnWorkshopModuleBuild);

		_normalTile = Resources.Load("Materials/Tile") as Material;
		_errorTile = Resources.Load("Materials/TileRed") as Material;
		_normalGhost = Resources.Load("Materials/Ghost") as Material;
		_errorGhost = Resources.Load("Materials/GhostRed") as Material;
		
		if (_machineGhost?.transform != null)
		{
			foreach (Transform child in _machineGhost?.transform)
			{
				child.GetComponent<Renderer>().material = Resources.Load("Materials/Ghost") as Material;
			}
		}
		_hit.point = new Vector3(4,0,40);
		_machineGhost.transform.position = _hit.point;
		GenerateMachineArea(_machineDatas.GetMachine(ManufacturerType.Dispenser));
		BuildMachine();
	}
	
	private void Update ()
	{

		if (Camera.main == null) return;
		
		_ray = Camera.main.ScreenPointToRay(Input.mousePosition); // raycast to game floor grid

		if (Input.GetButtonDown("Fire2"))
		{
			_building = false;
			ResetBuildPlanArea();
			_currentGridObjectType = GridObjectType.Default;
			_selectedTile = null;
			_currentTile = null;
		}
		
		if (Physics.Raycast(_ray, out _hit, 100) && !EventSystem.current.IsPointerOverGameObject())
		{
			if (_building)
			{
				_tilePos.Set((int) _hit.point.x + _offsetX, 0, (int) _hit.point.z + _offsetY); // making hit point an integer for tile cords usage
				
				if (_currentGridObjectType == GridObjectType.Machine || _currentGridObjectType == GridObjectType.Stockpile || _currentGridObjectType == GridObjectType.Workshop)
				{
					UpdateMachineBuilding();
				} 
				else if (_currentGridObjectType == GridObjectType.Product)
				{
					UpdatePartBuilding();
				}
				else if (_currentGridObjectType == GridObjectType.Module)
				{
					UpdateModuleBuilding();
				}
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				Worker engineer = new Worker((int) _hit.point.x, (int) _hit.point.z,Instantiate(AssetBundleManager.Instance.GetWorkersBundle().LoadAsset<GameObject>("NPC")));
				_workManager.AddWorker(engineer);
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha2))
			{

				Part aluminium = new Part(_manufacturerManager.GetPartData(PartType.Aluminium), (int) _hit.point.x,
					(int) _hit.point.z, 5000,
					Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(PartType.Aluminium.ToString()),
						new Vector3((int) _hit.point.x + 0.5f, 0, (int) _hit.point.z + 0.5f), Quaternion.identity) as GameObject);
				_gridObjectManager.AddPart(aluminium);
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha3))
			{			
				Part sheets = new Part(_manufacturerManager.GetPartData(PartType.Sheets), (int) _hit.point.x, (int) _hit.point.z,
					5000,
					Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(PartType.Sheets.ToString()),
						new Vector3((int) _hit.point.x + 0.5f, 0, (int) _hit.point.z + 0.5f), Quaternion.identity) as GameObject);
				_gridObjectManager.AddPart(sheets);
				
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				SetCurrentManufacturer(ManufacturerType.Stockpile);
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha5))
			{
				SetCurrentManufacturer(ManufacturerType.BladeCut);
			}
			
			if (Input.GetKeyDown(KeyCode.Alpha6))
			{
				SetCurrentManufacturer(ManufacturerType.SpinningFormer);
			}

		}

		if (_machineGhost != null)
		{

			if (_building && (!_machineGhost.activeInHierarchy || !_reserverArea.activeInHierarchy))
			{
				_machineGhost.SetActive(true);
				_reserverArea.SetActive(true);
			}
			else if (!_building && (_machineGhost.activeInHierarchy || _reserverArea.activeInHierarchy))
			{
				_machineGhost.SetActive(false);
				_reserverArea.SetActive(false);
			}
		}
	}
	
	// Getters

	public Machine GetCurrentMachine()
	{
		return _currentMachine;
	}
	
	public Part GetCurrentPart()
	{
		return _currentPart;
	}
	
	public Module GetCurrentModule()
	{
		return _currentModule;
	}
	
	// Setters
	
	// Methods

	private void UpdateMachineBuilding()
	{
		
		if (_currentGridObjectType == GridObjectType.Machine)
		{
			if (_tilePos.x > 0 + _currentMachine.GetMachineData().GetSize().x / 2 &&
			    _tilePos.x < _gridManager.GridSize().x - _currentMachine.GetMachineData().GetSize().x / 2 &&
			    _tilePos.z > 0 + _currentMachine.GetMachineData().GetSize().y / 2 &&
			    _tilePos.z < _gridManager.GridSize().y - _currentMachine.GetMachineData().GetSize().y / 2)
			{
				_outside = false;
			}
			else
			{
				_outside = true;
			}
		}

		if (_machineGhost != null)
		{
			_reserverArea.transform.position = _tilePos;
			_machineGhost.transform.position = _tilePos;
			
			if (_tilePos != _oldTilePos && !_deconstructMode) // only doing overlaping check when player changes position
			{
				_overlaping = _manufacturerManager.IsOverlapping(_hit.point, _currentMachine); //check for overlaping
				if (_overlaping || _outside) // if its overlaping then render error material
				{
					// TODO: make ghost models a single model
					foreach (Transform child in _machineGhost.transform) // going through all childs for now
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _errorGhost;
								}
								renderer.materials = mats;
							} 
							else if (child.GetComponent<SkinnedMeshRenderer>() != null)
							{
								SkinnedMeshRenderer skinRenderer = child.GetComponent<SkinnedMeshRenderer>();

								Material[] mats = skinRenderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _errorGhost;
								}
								skinRenderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _errorTile;
				}
				else // reset to normal material for ghost
				{
					foreach (Transform child in _machineGhost.transform)
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _normalGhost;
								}
								renderer.materials = mats;
							} 
							else if (child.GetComponent<SkinnedMeshRenderer>() != null)
							{
								SkinnedMeshRenderer skinRenderer = child.GetComponent<SkinnedMeshRenderer>();

								Material[] mats = skinRenderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _normalGhost;
								}
								skinRenderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _normalTile;
				}
				_oldTilePos = _tilePos;
			}

			if (Input.GetButtonDown("Fire1") && !_overlaping && !_deconstructMode && !_outside) // Building machine if its not overlaping anything
			{
				if (!_manufacturerManager.IsOverlapping(_hit.point, _currentMachine))
				{
					BuildMachine();
				}
			}

			if (Input.GetButtonDown("Fire1") && _deconstructMode && _hit.transform.CompareTag("Machine") && !_outside)
			{
				_manufacturerManager.RemoveMachine(int.Parse(_hit.transform.name));
			}

			if (Input.GetKeyDown(KeyCode.R) && !_deconstructMode) // Rotating the machine when changing X & Y cords
			{
				_rotated = !_rotated;
				ResetBuildPlanArea(); // resets machine for clean rotation
				_currentMachine.InvertSize();
				GenerateMachineArea(_currentMachine.GetMachineData());

				_machineGhost.transform.eulerAngles += new Vector3(0, 90, 0);
			}
		}
		else // no machine ghost defined so its a stockpile or a workshop area
		{
			if (Input.GetButtonDown("Fire1"))
			{

				if (_hit.collider.gameObject.CompareTag("Machine"))
				{
					_building = false;
					ResetBuildPlanArea();
					_currentGridObjectType = GridObjectType.Default;
					_selectedTile = null;
					_currentTile = null;
					_reserverArea.SetActive(false);
					_reserverArea.transform.position = new Vector3(-100,-100,-100);

					return;
				} 
				if (_hit.collider.gameObject.CompareTag("Workshop"))
				{
					_building = false;
					ResetBuildPlanArea();
					_currentGridObjectType = GridObjectType.Default;
					_selectedTile = null;
					_currentTile = null;
					_reserverArea.SetActive(false);
					_reserverArea.transform.position = new Vector3(-100,-100,-100);

					return;
				} 
				if (_hit.collider.gameObject.CompareTag("Stockpile"))
				{
					_building = false;
					ResetBuildPlanArea();
					_currentGridObjectType = GridObjectType.Default;
					_selectedTile = null;
					_currentTile = null;
					_reserverArea.SetActive(false);
					_reserverArea.transform.position = new Vector3(-100,-100,-100);

					
					return;
				}
				
				if (_selectedTile == null)
				{
					_selectedTile = _gridManager.GetTile((int) _tilePos.x, (int) _tilePos.z);
					if (!_selectedTile.Passable)
					{
						_selectedTile = null;
					}
					else
					{
						_reserverArea.SetActive(true);
						_currentTile = _selectedTile;
						_reserverArea.transform.position = new Vector3(_tilePos.x + 0.5f, 0, _tilePos.z + 0.5f);
						_reserverArea.transform.localScale = new Vector3(1, 0.1f, 1);
					}
				}
				// Build workshops/stockpile
				else if (_selectedTile != null && _currentTile != null && !_overlaping)
				{
					
					int posX = (int) startPoint.x;
					int posY = (int) startPoint.z;

					if (_currentGridObjectType.Equals(GridObjectType.Stockpile))
					{
						
						GameObject stockpileTile = new GameObject();
						stockpileTile.transform.parent = MachineList.transform;
						stockpileTile.transform.position = new Vector3(posX + _selectedAreaSize.x / 2, 0, posY + _selectedAreaSize.y / 2);
						stockpileTile.name = stockpileTile.GetInstanceID().ToString();
						stockpileTile.tag = "Stockpile";
						stockpileTile.layer = 9;
						
						GameObject stockpileUI = GameObject.Instantiate(Resources.Load("Prefabs/Areas/WarningAreaLimit"), _uiManager.GetCanvasFloor().transform) as GameObject;
						
						RectTransform UITransform = stockpileUI.GetComponent<RectTransform>();
						UITransform.sizeDelta = new Vector2(_selectedAreaSize.y,_selectedAreaSize.x);
						UITransform.position = new Vector3(posX + _selectedAreaSize.x / 2, 0.001f, posY + _selectedAreaSize.y / 2);

						Stockpile stockpile = new Stockpile(stockpileTile.GetInstanceID(), new Point(posX, posY), _selectedAreaSize, stockpileTile, stockpileUI);

						GridTile[,] tiles = new GridTile[(int) _selectedAreaSize.x,(int) _selectedAreaSize.y];

						for (int y = posY, iY = 0; y < _selectedAreaSize.y + posY; y++)
						{
							for (int x = posX, iX = 0; x < _selectedAreaSize.x + posX; x++)
							{
								tiles[iX, iY] = _gridManager.GetTile(x, y);
								tiles[iX, iY].SetCurrentGridObject(0);
								tiles[iX, iY].Service = true;
								iX++;
							}
							iY++;
						}
						
						stockpile.SetInventory(tiles);
						
						_gridObjectManager.AddStockpile(stockpile);
						_selectedTile = null;
						_currentTile = null;
						startPoint = new Vector3();
						_selectedAreaSize = new Vector2();
						_reserverArea.SetActive(false);
					} 
					else if (_currentGridObjectType.Equals(GridObjectType.Workshop))
					{
						GameObject workshopTile = new GameObject();
						workshopTile.transform.parent = MachineList.transform;
						workshopTile.transform.position = new Vector3(posX + _selectedAreaSize.x / 2, 0, posY + _selectedAreaSize.y / 2);
						workshopTile.name = workshopTile.GetInstanceID().ToString();
						workshopTile.tag = "Workshop";
						workshopTile.layer = 8;

						BoxCollider collider = workshopTile.AddComponent(typeof(BoxCollider)) as BoxCollider;
						collider.size =  new Vector3(_selectedAreaSize.x, 0.01f, _selectedAreaSize.y);
						
						GameObject workshopUI = GameObject.Instantiate(Resources.Load("Prefabs/Areas/AreaLimit"), _uiManager.GetCanvasFloor().transform) as GameObject;
						
						RectTransform UITransform = workshopUI.GetComponent<RectTransform>();
						UITransform.sizeDelta = new Vector2(_selectedAreaSize.y,_selectedAreaSize.x);
						UITransform.position = new Vector3(posX + _selectedAreaSize.x / 2, 0.001f, posY + _selectedAreaSize.y / 2);
						
						WorkshopData baseData = _workshopDatas.GetWorkshop(_currentManufacturerType);
									
						//TODO: remove hard coded way to assign color, add user based choice
						
						Image UIImage = workshopUI.GetComponent<Image>();
						switch (baseData.GetWorkshopType())
						{
							case ManufacturerType.Assembly:
								UIImage.color = new Color32(0,125,255,255);
								break;
							case ManufacturerType.Hydraulic:
								UIImage.color = new Color32(125,0,0,255);
								break;
							case ManufacturerType.Electronic:
								UIImage.color = new Color32(255,125,0,255);
								break;
							case ManufacturerType.MissionControl:
								UIImage.color = new Color32(255,0,0,255);
								break;
							case ManufacturerType.Recreation:
								UIImage.color = new Color32(64,125,0,255);
								break;
							case ManufacturerType.Software:
								UIImage.color = new Color32(0,0,0,255);
								break;
						}
						
						WorkshopData newData = new WorkshopData(
							baseData.GetName(),
							baseData.GetWorkshopType(),
							baseData.GetWorkshopParts(),
							baseData.GetWorkshopProducts(),
							baseData.GetWorkshopModules(),
							baseData.GetDescription()
						);
						Workshop workshop = new Workshop(newData, workshopTile.GetInstanceID(), posX, posY, (int) _selectedAreaSize.x, (int) _selectedAreaSize.y, workshopTile, workshopUI);
						GridTile[] serviceTiles = new[] {_gridManager.GetTile(posX, posY)};
						workshop.ReserveServiceTile(serviceTiles);		
						
						GridTile[] tiles = new GridTile[(int) _selectedAreaSize.x * (int) _selectedAreaSize.y];
		
						for (int  id = 0, y = posY; y < _selectedAreaSize.y + posY; y++)
						{
							for (int x = posX; x < _selectedAreaSize.x + posX; x++ )
							{
								tiles[id] = _gridManager.GetTile(x, y); // saving tiles on temp array 
								tiles[id].Reserved = true;
								id++;
							}
						}
						// Storing tiles on machine
						workshop.StoreTiles(tiles);
						
						workshopTile.name = workshop.ID.ToString();
						
						_manufacturerManager.AddWorkshop(workshop);
						_selectedTile = null;
						_currentTile = null;
						startPoint = new Vector3();
						_selectedAreaSize = new Vector2();
						_reserverArea.SetActive(false);
					}
				}
			}
			
			if (_selectedTile != null && (_currentTile.X != (int)_tilePos.x || _currentTile.Y != (int)_tilePos.z))
			{
				GridTile _currentTile = _gridManager.GetTile((int) _tilePos.x, (int) _tilePos.z);
					
				if 	(_selectedTile.X < _currentTile.X && _selectedTile.Y < _currentTile.Y)
				{
					_selectedAreaSize.Set(_currentTile.X - _selectedTile.X + 1, _currentTile.Y - _selectedTile.Y + 1);
					
					_reserverArea.transform.position = new Vector3(_selectedTile.X + _selectedAreaSize.x/2,0, _selectedTile.Y + _selectedAreaSize.y/2);
					startPoint = new Vector3(_selectedTile.X,0,_selectedTile.Y);
				} 
				else if (_selectedTile.X > _currentTile.X && _selectedTile.Y < _currentTile.Y)
				{
					_selectedAreaSize.Set(_selectedTile.X - _currentTile.X + 1, _currentTile.Y - _selectedTile.Y + 1);
					
					_reserverArea.transform.position = new Vector3(_selectedTile.X - _selectedAreaSize.x/2 + 1, 0, _selectedTile.Y + _selectedAreaSize.y/2);
					startPoint = new Vector3(_currentTile.X,0,_selectedTile.Y);
				}
				else if (_selectedTile.X > _currentTile.X && _selectedTile.Y > _currentTile.Y)
				{
					_selectedAreaSize.Set(_selectedTile.X - _currentTile.X + 1, _selectedTile.Y - _currentTile.Y + 1);
					
					_reserverArea.transform.position = new Vector3(_selectedTile.X - _selectedAreaSize.x/2 + 1, 0, _selectedTile.Y - _selectedAreaSize.y/2 + 1);
					startPoint = new Vector3(_currentTile.X,0,_currentTile.Y);
				}
				else if (_selectedTile.X < _currentTile.X && _selectedTile.Y > _currentTile.Y)
				{
					_selectedAreaSize.Set(_currentTile.X - _selectedTile.X + 1, _selectedTile.Y - _currentTile.Y + 1);
					
					_reserverArea.transform.position = new Vector3(_selectedTile.X + _selectedAreaSize.x/2, 0, _selectedTile.Y - _selectedAreaSize.y/2 + 1);
					startPoint = new Vector3(_selectedTile.X,0,_currentTile.Y);
				}
				
				_reserverArea.transform.localScale = new Vector3(_selectedAreaSize.x,0.1f,_selectedAreaSize.y);

				_overlaping = _manufacturerManager.IsOverlapping(startPoint, _selectedAreaSize);
								
				if (!_overlaping)
				{
					_reserverArea.GetComponent<Renderer>().material = _normalTile;
				}
				else
				{
					_reserverArea.GetComponent<Renderer>().material = _errorTile;
				}
			}
		}
	}

	private void UpdatePartBuilding()
	{
		if (_tilePos.x > 0 + _currentPart.GetPartData().GetSize().x / 2 &&
		    _tilePos.x < _gridManager.GridSize().x - _currentPart.GetPartData().GetSize().x / 2 &&
		    _tilePos.z > 0 + _currentPart.GetPartData().GetSize().y / 2 &&
		    _tilePos.z < _gridManager.GridSize().y - _currentPart.GetPartData().GetSize().y / 2)
		{
			_outside = true;
		}
		else
		{
			_outside = false;
		}
		
		if (_machineGhost != null)
		{
			_reserverArea.transform.position = _tilePos;
			_machineGhost.transform.position = _tilePos;

			if (_tilePos != _oldTilePos && !_deconstructMode && _outside) // only doing overlaping check when player changes position
			{
				_overlaping = _manufacturerManager.IsReverseOverlapping(_hit.point, _currentPart.GetPartData().GetSize(),_currentWorkshop); //check for overlaping
				
				// false when its all inside the workshop
				// true if any of the tiles hits a non reserved tile by the workshop
				
				if (!_overlaping)
				{
					// false so its inside the workshop, lets check again but instead look for a part inside
					if (_manufacturerManager.IsOverlapping(_hit.point, _currentPart))
					{
						_overlaping = true;
					}
				}
				
				if (_overlaping) // if its overlaping then render error material
				{
					// TODO: make ghost models a single model
					// TODO: make ghost models a single model
					foreach (Transform child in _machineGhost.transform) // going through all childs for now
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _errorGhost;
								}
								renderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _errorTile;
				}
				else // reset to normal material for ghost
				{
					foreach (Transform child in _machineGhost.transform)
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _normalGhost;
								}
								renderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _normalTile;
				}
				_oldTilePos = _tilePos;
			}

			if (Input.GetButtonDown("Fire1") && !_overlaping && !_deconstructMode && _outside) // Building part if its not overlaping anything
			{
				if (!_manufacturerManager.IsOverlapping(_hit.point, _currentPart))
				{
					BuildPart();
				}
			}

			if (Input.GetButtonDown("Fire1") && _deconstructMode && _hit.transform.CompareTag("Product") && _outside)
			{
				// TODO: add removing
				//_manufacturerManager.RemoveMachine(int.Parse(_hit.transform.name));
			}

			if (Input.GetKeyDown(KeyCode.R) && !_deconstructMode) // Rotating the part when changing X & Y cords
			{
				_rotated = !_rotated;
				ResetBuildPlanArea(); // resets part for clean rotation
				_currentPart.InvertSize();
				GeneratePartArea(_currentPart.GetPartData());

				_machineGhost.transform.eulerAngles += new Vector3(0, 90, 0);
			}
		}
	}

	private void UpdateModuleBuilding()
	{
		if (_tilePos.x > 0 + _currentModule.GetModuleData().GetSize().x / 2 &&
		    _tilePos.x < _gridManager.GridSize().x - _currentModule.GetModuleData().GetSize().x / 2 &&
		    _tilePos.z > 0 + _currentModule.GetModuleData().GetSize().y / 2 &&
		    _tilePos.z < _gridManager.GridSize().y - _currentModule.GetModuleData().GetSize().y / 2)
		{
			_outside = true;
		}
		else
		{
			_outside = false;
		}
		
		if (_machineGhost != null)
		{
			_reserverArea.transform.position = _tilePos;
			_machineGhost.transform.position = _tilePos;

			if (_tilePos != _oldTilePos && !_deconstructMode && _outside) // only doing overlaping check when player changes position
			{
				_overlaping = _manufacturerManager.IsReverseOverlapping(_hit.point, _currentModule.GetModuleData().GetSize(),_currentWorkshop); //check for overlaping
				
				// false when its all inside the workshop
				// true if any of the tiles hits a non reserved tile by the workshop
				
				if (!_overlaping)
				{
					// false so its inside the workshop, lets check again but instead look for a part inside
					if (_manufacturerManager.IsOverlapping(_hit.point, _currentModule))
					{
						_overlaping = true;
					}
				}
				
				if (_overlaping) // if its overlaping then render error material
				{
					// TODO: make ghost models a single model
					// TODO: make ghost models a single model
					foreach (Transform child in _machineGhost.transform) // going through all childs for now
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _errorGhost;
								}
								renderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _errorTile;
				}
				else // reset to normal material for ghost
				{
					foreach (Transform child in _machineGhost.transform)
					{
						if ((child.name != "SpawnPoint") && child.name != "Service")
						{
							Renderer renderer = child.GetComponent<Renderer>();

							if (renderer != null)
							{
								Material[] mats = renderer.materials;
								
								for (int i = 0; i < mats.Length; i++)
								{
									mats[i] = _normalGhost;
								}
								renderer.materials = mats;
							}
						}
					}
					_reserverArea.GetComponent<Renderer>().material = _normalTile;
				}
				_oldTilePos = _tilePos;
			}

			if (Input.GetButtonDown("Fire1") && !_overlaping && !_deconstructMode && _outside) // Building part if its not overlaping anything
			{
				if (!_manufacturerManager.IsOverlapping(_hit.point, _currentModule))
				{
					BuildModule();
				}
			}

			if (Input.GetButtonDown("Fire1") && _deconstructMode && _hit.transform.CompareTag("Product") && _outside)
			{
				// TODO: add removing
				//_manufacturerManager.RemoveMachine(int.Parse(_hit.transform.name));
			}

			if (Input.GetKeyDown(KeyCode.R) && !_deconstructMode) // Rotating the part when changing X & Y cords
			{
				_rotated = !_rotated;
				ResetBuildPlanArea(); // resets part for clean rotation
				_currentModule.InvertSize();
				GenerateModuleArea(_currentModule.GetModuleData());

				_machineGhost.transform.eulerAngles += new Vector3(0, 90, 0);
			}
		}
	}

	
	private void OnWorkshopProductBuild(PartType partType, int workshopID, bool discard)
	{
		if (!discard)
		{
			SetCurrentProduct(partType,workshopID);
		}
		else
		{
			
		}
	}
	
	private void OnWorkshopModuleBuild(ModuleType moduleType, int workshopID, bool discard)
	{
		if (!discard)
		{
			SetCurrentModule(moduleType,workshopID);
		}
		else
		{
			
		}
	}
	
	public void SetCurrentManufacturer(ManufacturerType type)
	{
		_currentPart = null;
		_currentModule = null;
		_currentManufacturerType = type;
		_deconstructMode = false;
		Destroy(_machineGhost);
		
		// TODO: fix this crappy way to determine type of building
		// this range of manufacturer types has ghosts
		if ((int)type <= 10)
		{
			_currentGridObjectType = GridObjectType.Machine;
			_machineGhost = Instantiate(AssetBundleManager.Instance.GetMachineBundle().LoadAsset<GameObject>(type.ToString()), new Vector3(-100, -100, -100),_reserverArea.transform.rotation) as GameObject;
			_machineGhost.name = type.ToString();
			_machineGhost.tag = "Untagged";
			_machineGhost.layer = 0;
			if (_machineGhost.GetComponent<BoxCollider>() != null)
			{
				_machineGhost.GetComponent<BoxCollider>().enabled = false;
			}

			if (_machineGhost?.transform != null)
			{
				foreach (Transform child in _machineGhost?.transform)
				{
					if (child.name != "SpawnPoint" && child.name != "Service")
					{
						Renderer renderer = child.GetComponent<Renderer>();

						if (renderer != null)
						{
							Material[] mats = renderer.materials;
								
							for (int i = 0; i < mats.Length; i++)
							{
								mats[i] = _normalGhost;
							}
							renderer.materials = mats;
						} 
						else if (child.GetComponent<SkinnedMeshRenderer>() != null)
						{
							SkinnedMeshRenderer skinRenderer = child.GetComponent<SkinnedMeshRenderer>();

							Material[] mats = skinRenderer.materials;
								
							for (int i = 0; i < mats.Length; i++)
							{
								mats[i] = _normalGhost;
							}
							skinRenderer.materials = mats;
						}
					}
				}
			}
			GenerateMachineArea(_machineDatas.GetMachine(type));

			//_uiManager.ShowInfoPanel();
		}
		else if (type.Equals(ManufacturerType.Stockpile))
		{
			_currentGridObjectType = GridObjectType.Stockpile;
		}
		else
		{
			_currentGridObjectType = GridObjectType.Workshop;
		}

		_building = true;

	}

	public void SetCurrentProduct(PartType productType, int workshopID)
	{
		_currentMachine = null;
		_deconstructMode = false;
		Destroy(_machineGhost);
				
		// TODO: change var name to a more standard one //

		_currentWorkshop = _manufacturerManager.GetWorkshop(workshopID);
		
		_machineGhost = Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(productType.ToString()), new Vector3(-100, -100, -100),_reserverArea.transform.rotation) as GameObject;
		_machineGhost.name = productType.ToString();
		
		if (_machineGhost?.transform != null)
		{
			foreach (Transform child in _machineGhost?.transform)
			{
				if (child.GetComponent<Renderer>() != null)
				{
					child.GetComponent<Renderer>().material = Resources.Load("Materials/Ghost") as Material;
				}

				if (child.GetComponent<SkinnedMeshRenderer>() != null)
				{
					for (int i = 0; i < child.GetComponent<SkinnedMeshRenderer>().materials.Length; i++)
					{
						child.GetComponent<SkinnedMeshRenderer>().materials[i] = Resources.Load("Materials/Ghost") as Material;
					}
				}
			}
		}
		GeneratePartArea(_manufacturerManager.GetPartData(productType));

		_uiManager.ShowInfoPanel();
		_currentGridObjectType = GridObjectType.Product;
		_building = true;
	}
	
	public void SetCurrentModule(ModuleType moduleType, int workshopID)
	{
		_currentMachine = null;
		_deconstructMode = false;
		Destroy(_machineGhost);
				
		// TODO: change var name to a more standard one //

		_currentWorkshop = _manufacturerManager.GetWorkshop(workshopID);
		
		_machineGhost = Instantiate(AssetBundleManager.Instance.GetModulesBundle().LoadAsset<GameObject>(moduleType.ToString()), new Vector3(-100, -100, -100),_reserverArea.transform.rotation) as GameObject;
		_machineGhost.name = moduleType.ToString();
		
		if (_machineGhost?.transform != null)
		{
			foreach (Transform child in _machineGhost?.transform)
			{
				if (child.GetComponent<Renderer>() != null)
				{
					child.GetComponent<Renderer>().material = Resources.Load("Materials/Ghost") as Material;
				}

				if (child.GetComponent<SkinnedMeshRenderer>() != null)
				{
					for (int i = 0; i < child.GetComponent<SkinnedMeshRenderer>().materials.Length; i++)
					{
						child.GetComponent<SkinnedMeshRenderer>().materials[i] = Resources.Load("Materials/Ghost") as Material;
					}
				}
			}
		}
		GenerateModuleArea(_manufacturerManager.GetModuleData(moduleType));

		_uiManager.ShowInfoPanel();
		_currentGridObjectType = GridObjectType.Module;
		_building = true;
	}
	
	// create machine nodes that are the base of the machine in the grid
	private void GenerateMachineArea(MachineData data)
	{
		Machine machine = new Machine(data,0,0,0);
		
		if (_currentMachine != machine && _rotated)
		{
			_currentMachine.InvertSize();
			_rotated = false;
		}
		_currentMachine = machine;
		// looking for even or odd machine size to add offset
		_offsetX = (int)machine.GetMachineData().GetSize().x % 2 != 0 ? 0.5f : 0; // looking for offset on X cord
		_offsetY = (int)machine.GetMachineData().GetSize().y % 2 != 0 ? 0.5f : 0; // looking for offset on Y cord
		
		_reserverArea.transform.localScale = new Vector3(machine.GetMachineData().GetSize().x,0.1f,machine.GetMachineData().GetSize().y); // making the planning area the same size of the machine
	}
	
	private void GeneratePartArea(PartData data)
	{
		Part product = new Part(data,0,0,0);
		
		if (_currentPart != product && _rotated)
		{
			_currentPart.InvertSize();
			_rotated = false;
		}
		_currentPart = product;
		// looking for even or odd machine size to add offset
		_offsetX = (int)_currentPart.GetPartData().GetSize().x % 2 != 0 ? 0.5f : 0; // looking for offset on X cord
		_offsetY = (int)_currentPart.GetPartData().GetSize().y % 2 != 0 ? 0.5f : 0; // looking for offset on Y cord
		
		_reserverArea.transform.localScale = new Vector3(_currentPart.GetPartData().GetSize().x,0.1f,_currentPart.GetPartData().GetSize().y); // making the planning area the same size of the machine
	}
	
	private void GenerateModuleArea(ModuleData data)
	{
		Module mod = new Module(data,0,0,0);
		
		if (_currentModule != mod && _rotated)
		{
			_currentModule.InvertSize();
			_rotated = false;
		}
		_currentModule = mod;
		// looking for even or odd machine size to add offset
		_offsetX = (int)_currentModule.GetModuleData().GetSize().x % 2 != 0 ? 0.5f : 0; // looking for offset on X cord
		_offsetY = (int)_currentModule.GetModuleData().GetSize().y % 2 != 0 ? 0.5f : 0; // looking for offset on Y cord
		
		_reserverArea.transform.localScale = new Vector3(_currentModule.GetModuleData().GetSize().x,0.1f,_currentModule.GetModuleData().GetSize().y); // making the planning area the same size of the machine
	}
	
	// resets the machine area for a new one
	private void ResetBuildPlanArea()
	{
		_reserverArea.transform.position = new Vector3(-100,-100,-100);
	}

	private void BuildMachine()
	{
		if (_machineGhost.GetComponent<BoxCollider>() != null)
		{
			_machineGhost.GetComponent<BoxCollider>().enabled = true;
		}

		// getting the first tile from the machine
		int posX = (int) _hit.point.x - ((int) (_currentMachine.GetMachineData().GetSize().x / 2));
		int posY = (int) _hit.point.z - ((int) (_currentMachine.GetMachineData().GetSize().y / 2));
		
		GameObject finalMachineGo = Instantiate(
			AssetBundleManager.Instance.GetMachineBundle().LoadAsset<GameObject>(_currentMachine.GetMachineData().GetMachineType().ToString()),
			_machineGhost.transform.position,
			_machineGhost.transform.rotation,
			MachineList.transform
		) as GameObject; // In-game gameobject for the recently created machine

		MachineData newData = new MachineData(
			_currentMachine.GetMachineData().GetName(),
			_currentMachine.GetMachineData().GetMachineType(),
			(int)_currentMachine.GetMachineData().GetSize().x,
			(int)_currentMachine.GetMachineData().GetSize().y,
			_currentMachine.GetMachineData().GetHealth(),
			_currentMachine.GetMachineData().GetPrice(),
			_currentMachine.GetMachineData().GetHours(),
			_currentMachine.GetMachineData().GetMachineParts(),
			_currentMachine.GetMachineData().GetDescription()
		);
		
		Machine finalMachine = new Machine(newData,finalMachineGo.GetInstanceID(),posX,posY,finalMachineGo); // finalMachine is created and stored into a component list
		finalMachineGo.name = finalMachine.ID.ToString();
		// check what tiles the machine will use
		// looking into each tile

		GridTile[] tiles = new GridTile[(int) _currentMachine.GetMachineData().GetSize().x *
		                                (int) _currentMachine.GetMachineData().GetSize().y];
		
		for (int  id = 0, y = posY; y < _currentMachine.GetMachineData().GetSize().y + posY; y++)
		{
			for (int x = posX; x < _currentMachine.GetMachineData().GetSize().x + posX; x++ )
			{
				tiles[id] = _gridManager.GetTile(x, y); // saving tiles on temp array 
				id++;
			}
		}
		// Storing tiles on machine
		finalMachine.ReserveTiles(tiles);

		
		if (finalMachineGo != null)
		{
			
			int serviceTilesCount = 0;

			foreach (Transform child in finalMachineGo.transform)
			{
				if (child.name == "Service")
				{
					serviceTilesCount++;
				}
			}
			
			GridTile[] serviceTiles = new GridTile[serviceTilesCount];

			int i = 0;
			
			foreach (Transform child in finalMachineGo.transform)
			{
				if (child.name == "Service")
				{
					serviceTiles[i] = _gridManager.GetTile((int) child.position.x, (int) child.position.z);
					i++;
				}
			}
			
			finalMachine.ReserveServiceTile(serviceTiles);
		}

		_manufacturerManager.AddMachine(finalMachine); // saving machine on machine manager
		
		var graphToScan = AstarPath.active.data.gridGraph;
		AstarPath.active.Scan(graphToScan);
		
		if (_machineGhost.GetComponent<BoxCollider>() != null)
		{
			_machineGhost.GetComponent<BoxCollider>().enabled = false;
		}
	}

	private void BuildPart()
	{
		// getting the first tile from the machine
		int posX = (int) _hit.point.x - ((int) (_currentPart.GetPartData().GetSize().x / 2));
		int posY = (int) _hit.point.z - ((int) (_currentPart.GetPartData().GetSize().y / 2));
				
		GameObject finalPartGo = Instantiate(
			AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(_currentPart.GetPartData().GetPartType().ToString()),
			_machineGhost.transform.position,
			_machineGhost.transform.rotation,
			MachineList.transform
		) as GameObject; // In-game gameobject for the recently created machine
		
		PartData newData = new PartData(
			_currentPart.GetPartData().GetName(),
			_currentPart.GetPartData().GetHealth(),
			_currentPart.GetPartData().GetAmountLimit(),
			_currentPart.GetPartData().GetPartType(),
			_currentPart.GetPartData().IsResource(),
			_currentPart.GetPartData().GetSize(),
			_currentPart.GetPartData().GetDescription()
		);
		
		Part finalPart = new Part(newData,posX,posY,finalPartGo);
		finalPartGo.name = finalPart.ID.ToString();
		// check what tiles the part will use
		// looking into each tile
		for (int  id = 0, y = posY; y < _currentPart.GetPartData().GetSize().y + posY; y++)
		{
			for (int x = posX; x < _currentPart.GetPartData().GetSize().x + posX; x++ )
			{
				finalPart.ReserveTile(_gridManager.GetTile(x,y)); // saving tiles on machine 
				id++;
			}
		}

		if (finalPartGo != null)
		{
			foreach (Transform child in finalPartGo.transform)
			{
				if (child.name == "Service")
				{
					finalPart.ReserveServiceTile(_gridManager.GetTile((int) child.position.x, (int) child.position.z));
				}
			}
		}
				
		Messenger<Part, PartType, int>.Broadcast("new WorkshopPartSelected", finalPart, finalPart.GetPartData().GetPartType(),  _currentWorkshop.ID);
		
		var graphToScan = AstarPath.active.data.gridGraph;
		AstarPath.active.Scan(graphToScan);
	}
	
	private void BuildModule()
	{
		// getting the first tile from the module
		int posX = (int) _hit.point.x - ((int) (_currentModule.GetModuleData().GetSize().x / 2));
		int posY = (int) _hit.point.z - ((int) (_currentModule.GetModuleData().GetSize().y / 2));
				
		GameObject finalModGo = Instantiate(
			AssetBundleManager.Instance.GetModulesBundle().LoadAsset(_currentModule.GetModuleData().GetModuleType().ToString()),
			_machineGhost.transform.position,
			_machineGhost.transform.rotation,
			MachineList.transform
		) as GameObject; // In-game gameobject for the recently created machine
		
		ModuleData newData = new ModuleData(
			_currentModule.GetModuleData().GetName(),
			_currentModule.GetModuleData().GetHealth(),
			_currentModule.GetModuleData().GetModuleType(),
			_currentModule.GetModuleData().GetSize(),
			_currentModule.GetModuleData().GetDescription()
		);
		
		Module finalMod = new Module(newData,posX,posY,finalModGo);
		// check what tiles the module will use
		// looking into each tile
		for (int  id = 0, y = posY; y < _currentModule.GetModuleData().GetSize().y + posY; y++)
		{
			for (int x = posX; x < _currentModule.GetModuleData().GetSize().x + posX; x++ )
			{
				finalMod.ReserveTile(_gridManager.GetTile(x,y)); // saving tiles on machine 
				id++;
			}
		}

		if (finalModGo != null)
		{
			foreach (Transform child in finalModGo.transform)
			{
				if (child.name == "Service")
				{
					finalMod.ReserveServiceTile(_gridManager.GetTile((int) child.position.x, (int) child.position.z));
				}
			}
		}
				
		//Messenger<Module, ModuleType, int>.Broadcast("new WorkshopPartSelected", finalMod, finalMod.GetModuleData().GetModuleType(),  _currentWorkshop.ID);
		
		var graphToScan = AstarPath.active.data.gridGraph;
		AstarPath.active.Scan(graphToScan);
	}

	public bool IsInBuildMode()
	{
		return _building;
	}
	
}