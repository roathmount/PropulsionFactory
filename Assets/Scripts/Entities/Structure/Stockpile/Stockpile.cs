﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class Stockpile : GridObject {

	private GridTile[,] _invetory;
	private Vector2 _size;
	
	private GameObject _gameObject,_uiGameObject;

	public Stockpile(int id, Point location, Vector2 size, GameObject gameObject, GameObject uiGameObject) : base(id, location, GridObjectType.Stockpile)
	{
		_size = size;
		_invetory = new GridTile[(int) size.x,(int) size.y];
		_uiGameObject = uiGameObject;
		_gameObject = gameObject;
	}
	
	public Stockpile(int id, Point location, int sizeX, int sizeY, GameObject gameObject, GameObject uiGameObject) : base(id, location, GridObjectType.Stockpile)
	{
		_size = new Vector2(sizeX, sizeY);
		_invetory = new GridTile[sizeX, sizeY];
		_uiGameObject = uiGameObject;
		_gameObject = gameObject;
	}

	public Stockpile(int id, int x, int y) : base(id, x, y, GridObjectType.Stockpile)
	{
	}
	
	// Getters

	// Returns a free tile inside stockpile
	public GridTile GetDepositTile()
	{
		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				if (!_invetory[x, y].Reserved) return _invetory[x, y];
			}
		}
		return null;
	}

	// TODO: (stockpile) improve tile search
	public GridTile GetDepositTile(Point location)
	{
		if (_invetory.Length == 0) return null;
		GridTile bestTile = null;
		
		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				GridTile tile = _invetory[x, y];
				
				if (tile.Reserved) continue;
				if (bestTile != null)
				{
					if (Point.Distance(location, tile.Location) <
					    Point.Distance(location, bestTile.Location))
					{
						bestTile = tile;
					}
				}
				else
				{
					bestTile = tile;
				}
			}
		}
		return bestTile;
	}
	
	public GridTile[] GetDepositTiles(Vector2 partSize, Point location)
	{
		if (_invetory.Length == 0) return null;
		GridTile[] bestTiles = new GridTile[(int) partSize.x * (int) partSize.y];
		GridTile[] currentTiles = new GridTile[(int) partSize.x * (int) partSize.y];

		for (int y = 0; y <= _size.y - partSize.y; y++)
		{
			for (int x = 0; x <= _size.x - partSize.x; x++)
			{

				if (_invetory[x, y].Reserved) continue;

				bool avaliableArea = true;

				// Check if the new area is avaliable
				for (int subY = 0, tileID = 0; subY < partSize.y; subY++)
				{
					for (int subX = 0; subX < partSize.x; subX++)
					{
						GridTile subTile = _invetory[x+subX, y+subY];
						if (subTile == null) continue;
						if (subTile.Reserved)
						{
							avaliableArea = false;
							currentTiles = new GridTile[(int) partSize.x * (int) partSize.y];
						}
						else
						{
							currentTiles[tileID] = subTile;
							tileID++;
						}
					}
				}
				
				// Check if the new area is better than the last one

				if (avaliableArea)
				{
					if (bestTiles[0] != null)
					{
						if ((Point.Distance(location, currentTiles[0].Location) < Point.Distance(location, bestTiles[0].Location)))
						{
							bestTiles = (GridTile[])currentTiles.Clone();
						}
					}
					else
					{
						bestTiles = (GridTile[])currentTiles.Clone();
					}
				}
			}
		}
		return bestTiles;
	}
	
	public bool HasTile(int id)
	{
		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				if (_invetory[x, y].ID == id)
				{
					return true;
				}
			}
		}

		return false;
	}

	
	public bool HasGridObject(int id)
	{
		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				if (_invetory[x, y].GetCurrentGridObject() == id)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	// Checks if a tile is reserved
	public bool IsTileReserved(int id)
	{
		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				if (_invetory[x, y].GetCurrentGridObject() != id) continue;
				if (_invetory[x, y].Reserved)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	
	public GameObject GetBody()
	{
		return _gameObject;
	}
	
	// Setters

	public void SetInventory(GridTile[,] array)
	{
		_invetory = array;
	}
	
	// Methods

	public void ReserveTile(GridTile tile)
	{
		if (tile != null) tile.Reserved = true;
	}
	
	// TODO: add size consideration
	public bool Store(GridObject obj, Vector2 objSize)
	{
		GridTile orig = _invetory[0, 0];
		
		// Obj position is first tile of the reserved area
		if(obj.X < orig.X && obj.X > orig.X+_size.x) return false;
		if(obj.Y < orig.Y && obj.Y > orig.Y+_size.y) return false;

		// Get first tile
		GridTile _baseTile = _invetory[obj.X - orig.X, obj.Y - orig.Y];
		
		GridTile[] subTiles = new GridTile[(int) (objSize.x * objSize.y)];
		subTiles[0] = _baseTile;
		for (int y = 0, subIndex = 1; y < objSize.y; y++)
		{
			for (int x = 0; x < objSize.x; x++)
			{
				GridTile subTile = _invetory[(obj.X - orig.X) + x, (obj.Y - orig.Y) + y];
				if (subTile != null)
				{
					if (subTile.Reserved && subTile.GetCurrentGridObject() == 0)
					{
						if(subTile.Location == _baseTile.Location) continue;
						subTiles[subIndex] = subTile;
						subIndex++;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}

		for (int i = 0; i < subTiles.Length; i++)
		{
			GridTile targetTile = subTiles[i];
			_invetory[targetTile.X - orig.X, targetTile.Y - orig.Y].SetCurrentGridObject(obj.ID);
		}
		
		return true;
	}
	
	
	// Withdraw (remove) stored gridObject from tile
	public void Withdraw(GridObject obj, Vector2 size)
	{
		GridTile orig = _invetory[0, 0];
		
		if(obj.X < orig.X && obj.X > orig.X+_size.x) return;
		if(obj.Y < orig.Y && obj.Y > orig.Y+_size.y) return;

		for (int y = 0; y < size.y; y++)
		{
			for (int x = 0; x < size.x; x++)
			{
				GridTile subTile = _invetory[(obj.X - orig.X) + x, (obj.Y - orig.Y) + y];
				if (subTile?.GetCurrentGridObject() == obj.ID)
				{
					subTile.Reserved = false;
					subTile.SetCurrentGridObject(0);
				}

			}
		}
	}
	
	// TODO: (stockpile) only let remove stockpile when empty
	public void Remove()
	{

		for (int y = 0; y < _size.y; y++)
		{
			for (int x = 0; x < _size.x; x++)
			{
				_invetory[x, y].Passable = true;
			}
		}
		_invetory = null;
		Object.Destroy(_uiGameObject);
		Object.Destroy(_gameObject);
	}
	
}
