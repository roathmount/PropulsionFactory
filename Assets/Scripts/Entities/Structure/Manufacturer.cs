﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class Manufacturer : GridObject {


    protected int WorkAmount = 0;
    private int _totalWorkAmount;

    public int TotalWorkAmount
    {
        get { return _totalWorkAmount; }
        set { _totalWorkAmount = value; }
    }

    protected bool Active, Empty, Operated;
    
    protected GridTile[] _tiles;
    protected GridTile[] _serviceTiles;
    protected List<Part> _resources;
    
    protected ManufacturerState _currentManufacturerStateState;
    protected GameObject _gameObject;

    public GameObject GameObject
    {
        get { return _gameObject; }
        set { _gameObject = value; }
    }

    protected Recipe _currentRecipe;
    protected Part _storedPart;
    protected Worker _currentOperator;

    public Manufacturer(int id, int x, int y, GridObjectType gridObjectType) : base(id, x, y, gridObjectType)
    {
        _resources = new List<Part>();
        _currentManufacturerStateState = ManufacturerState.Idle;
        _gameObject = null;
        Active = false;
        Empty = true;
        Operated = false;
        _storedPart = null;
        _currentOperator = null;
    }
    
    public Manufacturer(int id, int x, int y, GridObjectType gridObjectType, GameObject body) : base(id, x, y, gridObjectType)
    {
        _resources = new List<Part>();
        _currentManufacturerStateState = ManufacturerState.Idle;
        _gameObject = body;
        _gameObject.name = ID.ToString();
        Active = false;
        Empty = true;
        Operated = false;
        _currentOperator = null;
    }
    
    // Getters
    
    public int GetWorkAmount()
    {
        return WorkAmount;
    }

    public int GetProgress()
    {
        if (_totalWorkAmount <= 0)
        {
            return 0;
        }
        int percentage = 100 - ((WorkAmount * 100) / _totalWorkAmount);
        return percentage;
    }
    
    public bool IsActive()
    {
        return Active;
    }

    public bool IsEmpty()
    {
        return Empty;
    }

    public bool IsOperated()
    {
        return Operated;
    }
    
    public Part GetResource(PartType type)
    {
        return _resources.FirstOrDefault(resource => resource?.GetPartData().GetPartType() == type);
    }

    public Recipe GetRecipe()
    {
        return _currentRecipe;
    }

    public ManufacturerState GetManufacturerState()
    {
        return _currentManufacturerStateState;
    }

    public GridTile[] GetTiles()
    {
        return _tiles;
    }
    
    public GridTile GetServiceTile()
    {
        return _serviceTiles[0];
    }
    
    // Looks into each tile ID reserved on machine and returns true if the ID passed along is inside the list
    public bool IsTileReserved(int id)
    {
        return _tiles.Any(t => t.ID == id);
    }
    
    public Part GetStoredPart()
    {
        return _storedPart;
    }
    
    public Worker GetCurrentOperator()
    {
        return _currentOperator;
    }

    // Setters
    
    public void SetActive(bool value)
    {
        Active = value;
    }

    public void SetOperated(bool value)
    {
        Operated = value;
    }

    public void SetManufacturerState(ManufacturerState state)
    {
        _currentManufacturerStateState = state;
    }
    
    public void SetCurrentOperator(Worker worker)
    {
        _currentOperator = worker;
    } 

    public void AddWork(int amount)
    {
        if ((WorkAmount - amount) <= 0)
        {
            WorkAmount = 0;
            FinishManufacture();
        }
        else
        {
            WorkAmount -= amount; 
        }
    }

    // TODO: change default 1/4 of resource limit as production extraction
    
    public int UseResource(PartType resourceType)
    {
        // Look through all listed resources

        foreach (var resource in _resources)
        {
            // Check for a empty space
            if (resource != null && resource.GetPartData().GetPartType() == resourceType && resource.GetPartAmount() > 0)
            {
                float amount = (float) resource.GetPartData().GetAmountLimit() / 4;
                if (amount > 0 && amount <= 1)
                {
                    amount = 1;
                }
                return resource.UsePart((int)amount);
            }
        }
        return 0;
    }

    public void RemoveResource(PartType resourceType)
    {
        Part res = null;
        // Look through all listed resources
        foreach (var resource in _resources)
        {
            // Check if the added resource already exits on the list
            if (resource != null && resource.GetPartData().GetPartType() == resourceType)
            {
                // Add it to list
                res = resource;
            }
        }
        _resources.Remove(res);
    }

    public int AddResource(PartType resourceType, int amount)
    {
        // Look through all listed resources
        foreach (var resource in _resources)
        {
            // Check for a empty space
            if (resource != null && resource.GetPartData().GetPartType() == resourceType)
            {
                resource.SetQueued(false);
                int leftover = resource.AddAmount(amount);
                
                // If all resources are OK then continue working
                SetManufacturerState(ManufacturerState.Working);
                
                // Return surplus
                return leftover;
            }
        }
        // If all resources are OK then continue working
        SetManufacturerState(ManufacturerState.Working);
        // Return nothing
        return 0;
    }

    public void AddResource(Part resource)
    {
        // Look through all listed resources
        _resources.Add(resource);
        
        // If all resources are OK then continue working
        SetManufacturerState(ManufacturerState.Working);
    }    
    
    // Methods

    public virtual void Work()
    {
        
    }

    public virtual void ManufacturePart(PartData data, Recipe recipe)
    {
        
    }

    protected virtual void FinishManufacture()
    {
        _totalWorkAmount = 0;
    }
    
    // Returns true when a all resources empty
    protected bool CheckResources()
    {
        bool isEmpty = true;
        foreach (var resource in _resources)
        {
            if (resource.GetPartAmount() != 0)
            {
                isEmpty = false;
            }
        }

        return isEmpty;
    }

    public void ClearResources()
    {
        _resources.Clear();
    }
    
    // Adds to the ID list the tiles received
    public void ReserveTiles(GridTile[] tiles)
    {
        if (_tiles != null)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].Passable = false;
            }
        
            GridTile[] oldTiles = _tiles;
            _tiles = new GridTile[oldTiles.Length + tiles.Length];
            Array.Copy(oldTiles, _tiles, oldTiles.Length);
            Array.Copy(tiles, 0, _tiles, oldTiles.Length, tiles.Length);
        
        }
        else
        {
            _tiles = tiles;
            for (int i = 0; i < _tiles.Length; i++)
            {
                tiles[i].Passable = false;
            }
        }

    }
    
    public virtual void StoreTiles(GridTile[] tiles)
    {
        if (_tiles != null)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].Reserved = false;
            }
            
            GridTile[] oldTiles = _tiles;
            _tiles = new GridTile[oldTiles.Length + tiles.Length];
            Array.Copy(oldTiles, _tiles, oldTiles.Length);
            Array.Copy(tiles, 0, _tiles, oldTiles.Length, tiles.Length);
        }
        else
        {
            _tiles = tiles;
            for (int i = 0; i < _tiles.Length; i++)
            {
                tiles[i].Reserved = false;
            }
        }
    }
    
    public void ReserveServiceTile(GridTile[] tiles)
    {
        if (_serviceTiles != null)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].Passable = false;
            }
            
            GridTile[] oldTiles = _serviceTiles;
            _serviceTiles = new GridTile[oldTiles.Length + tiles.Length];
            Array.Copy(oldTiles, _serviceTiles, oldTiles.Length);
            Array.Copy(tiles, 0, _serviceTiles, oldTiles.Length, tiles.Length);
        }
        else
        {
            _serviceTiles = tiles;
            for (int i = 0; i < _serviceTiles.Length; i++)
            {
                tiles[i].Passable = false;
            }
        }
    }
    
    public virtual void Remove()
    {
        foreach (var tile in _tiles)
        {
            tile.Passable = true;
            tile.Reserved = false;
        }
        foreach (var tile in _serviceTiles)
        {
            tile.Service = false;
        }
        _tiles = null;
        _serviceTiles = null;
        Object.Destroy(_gameObject);
    }
}
