﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : GridObject
{

    private ModuleData _data;
    
    private GameObject _gameObject;
    
    protected List<GridTile> _tiles;
    protected List<GridTile> _serviceTiles;
            
    public Module(ModuleData newData, int id, int x, int y) : base(id, x, y, GridObjectType.Module)
    {
        _data = newData;
        _gameObject = null;
    }
    
    public Module(ModuleData newData, int x, int y, GameObject body) : base(body.GetInstanceID(), x, y, GridObjectType.Module)
    {
        _data = newData;
        _gameObject = body;
        _gameObject.name = ID.ToString();
    }
    
    // Getters
    
    public GameObject GetBody()
    {
        return _gameObject;
    }
    
    public ModuleData GetModuleData()
    {
        return _data;
    }
    
    // Setters
    
    // Methods
    
    // Adds to the ID list the tiles received
    public void ReserveTile(GridTile tile)
    {
        if (_tiles == null)
        {
            _tiles = new List<GridTile>();
        }
        tile.Passable = false;
        _tiles.Add(tile);
    }

    public void ReserveServiceTile(GridTile tile)
    {
        if (_serviceTiles == null)
        {
            _serviceTiles = new List<GridTile>();
        }
        tile.Service = true;
        _serviceTiles.Add(tile);
    }
    
    public void UnReserveTiles()
    {
        if (_tiles == null) return;
        foreach (var tile in _tiles)
        {
            tile.Passable = true;
            tile.Reserved = false;
        }
        _tiles.Clear();
        _tiles = null;
    }
	
    public void UnReserveServiceTiles()
    {
        if (_serviceTiles == null) return;
        foreach (var tile in _serviceTiles)
        {
            tile.Service = false;
        }
        _serviceTiles.Clear();
        _serviceTiles = null;
    }
    
    // Inverts X and Y cords of the module if rotated
    public void InvertSize()
    {
        var tempX = _data.GetSize().y;
        var tempY = _data.GetSize().x;

        _data.SetSize((int)tempX,(int)tempY);
    }
    
    public void Remove()
    {
        Object.Destroy(_gameObject);
        UnReserveTiles();
        UnReserveServiceTiles();
        _gameObject = null;
    }
}
