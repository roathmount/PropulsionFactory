﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Object;
using Object = UnityEngine.Object;

public class Workshop : Manufacturer
{
    private WorkshopData _data;

	private List<Product> _storedProducts;

    private bool _multipleProduction;

    private PartType _selectedPartType;
    
    private GameObject _uiGameObject;
    
    private Vector2 _size;
	
    // Constructors used for building not for final machine placement
    public Workshop(WorkshopData newData, int id, int x, int y, int sizeX, int sizeY) : base(id, x, y, GridObjectType.Workshop)
    {
        _data = newData;
        _selectedPartType = PartType.None;
        _size = new Vector2(sizeX,sizeY);
    }
    
    public Workshop(WorkshopData newData, int id, int x, int y, int sizeX, int sizeY, GameObject body, GameObject UIGameObject) : base(id, x, y, GridObjectType.Workshop, body)
    {
        _data = newData;
        _selectedPartType = PartType.None;
        _uiGameObject = UIGameObject;
        _size = new Vector2(sizeX,sizeY);
        
    }
	
	// Getters

    public List<Product> GetStoredProducts()
    {
        return _storedProducts;
    }
    
	public Product GetProduct(PartType part)
	{
	    return _storedProducts.FirstOrDefault(t => t.GetPart().GetPartData().GetPartType() == part);
	}

    public Product GetProduct(int id)
    {
        return _storedProducts.FirstOrDefault(product => product.ID == id);
    }

	public WorkshopData GetWorkshopData()
	{
		return _data;
	}

    public PartType GetSelectedPartType()
    {
        return _selectedPartType;
    }
    
    public Vector2 GetSize()
    {
        return _size;
    }
    
    // Setters
    
    public void SetSelectedPartType(PartType newSelectedPartTypePart)
    {
        _selectedPartType = newSelectedPartTypePart;
    }
	
    // Methods

    public override void Work()
    {
        if (_multipleProduction)
        {
            if (_storedProducts.Count <= 0 && !Empty)
            {
                if (!_storedPart.IsQueued())
                {
                    Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
                }
                return;
            }

            foreach (var product in _storedProducts.ToArray())
            {
                if (product.IsFinished())
                {
                    // Only finish part when there isn't another one waiting to be removed

                    if (_storedPart == null) FinishProductManufacture(product);
                    continue;
                }
                
                foreach (var item in product.GetRecipe().GetItems().ToArray())
                {
                    if (item.GetItemAmount() <= 0)
                    {
                        product.GetRecipe().RemoveItem(item);
                        continue;
                    }
                    // Get the resources used in the manufacturing process
                    int usedResources = UseResource(item.GetItemType());

                    // TODO: (recipe system) Also send recharge request when resource amount is lower than 1/4 of resource amount limit
                    // No resource amount left for manufacturing, request recharge
                    Part resource = GetResource(item.GetItemType());
                    
                    if (usedResources == 0 && !resource.IsQueued())
                    {
                        // Set machine idle while waiting for resources
                        SetManufacturerState(ManufacturerState.Idle);
                        // Request
                        // TODO: try to make this call standard for both machines and workshop (manufacturer)
                        Messenger<Manufacturer, RecipeItem>.Broadcast("new ManufacturerRequestRecharge", this, item);
                        continue;
                    }
                    
                    // Remove amount from recipe item using stored resource amount
                    int recoveredResources = item.RemoveItemAmount(usedResources);
                    product.AddProgress(usedResources + recoveredResources);
                    // Check if all resources sent were used
                    if (recoveredResources > 0)
                    {
                        // Add surplus of resources back into the list
                        AddResource(item.GetItemType(), recoveredResources);
                    }
                }
            }
        }
        else
        {
            if (_storedPart != null && !Empty)
            {
                if (!_storedPart.IsQueued())
                {
                    Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
                }
                return;
            }

            foreach (var item in _currentRecipe.GetItems().ToArray())
            {
                if (item.GetItemAmount() <= 0)
                {
                    _currentRecipe.RemoveItem(item);
                    continue;
                }
                // Get the resources used in the manufacturing process
                int usedResources = UseResource(item.GetItemType());

                // TODO: (recipe system) Also send recharge request when resource amount is lower than 1/4 of resource amount limit
                // No resource amount left for manufacturing, request recharge
                Part resource = GetResource(item.GetItemType());

                if (usedResources == 0 && !resource.IsQueued())
                {
                    // Set machine idle while waiting for resources
                    SetManufacturerState(ManufacturerState.Idle);
                    // Request
                    // TODO: try to make this call standard for both machines and workshop (manufacturer)
                    Messenger<Manufacturer, RecipeItem>.Broadcast("new ManufacturerRequestRecharge", this, item);
                    continue;
                }

                // Remove amount from recipe item using stored resource amount
                int recoveredResources = item.RemoveItemAmount(usedResources);
                AddWork(usedResources + recoveredResources);
                // Check if all resources sent were used
                if (recoveredResources > 0)
                {
                    // Add surplus of resources back into the list
                    AddResource(item.GetItemType(), recoveredResources);
                }
            }
        }
    }

    public void ManufacturePart(PartData data, Recipe recipe)
    {
        _multipleProduction = false;
        _storedProducts = null;
        GameObject partGo = Instantiate(AssetBundleManager.Instance.GetPartsBundle().LoadAsset<GameObject>(data.GetPartType().ToString()),_gameObject.transform) as GameObject;
        _storedPart = new Part(data, 0, 0,partGo);
        partGo.SetActive(false);
        // Set current part recipe
        _currentRecipe = recipe;
        // TODO: (Recipe system) Check for already stored resources to use, request them remove if they dont apply to the current recipe

        WorkAmount = 0;
    
        foreach (var item in _currentRecipe.GetItems())
        {
            WorkAmount += item.GetItemAmount();
        }

        TotalWorkAmount = WorkAmount;
        
        SetManufacturerState(ManufacturerState.Working);
    }
    
    public void ManufactureProduct(Part part, Recipe recipe)
    {
        _multipleProduction = true;
        _storedPart = null;
        if (_storedProducts == null)
        {
            _storedProducts = new List<Product>();
        }
        Product product = new Product(part.ID, this.X, this.Y, part, recipe);
        _storedProducts.Add(product);
        
        SetManufacturerState(ManufacturerState.Working);
    }

    protected override void FinishManufacture()
    {
        base.FinishManufacture();
        // Machine finished work so it becomes idle
        SetManufacturerState(ManufacturerState.Idle);
        
        Vector3 spawnPoint = new Vector3();
        Quaternion rotation = new Quaternion();
        
        foreach (Transform children in _gameObject.transform)
        {
            if (children.name == "SpawnPoint")
            {
                spawnPoint = children.transform.position;
                rotation = children.transform.rotation;
            }
        }
        _storedPart.GetBody().SetActive(true);
        _storedPart.GetBody().transform.position = spawnPoint;
        _storedPart.GetBody().transform.rotation = rotation;
        _storedPart.SetFinish(Quality.Normal);
        _storedPart.AddAmount(_storedPart.GetPartData().GetAmountLimit());
        Empty = false;
        
        // Being operations to make workshop avaliable again
        Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
    }
    
    private void FinishProductManufacture(Product product)
    {
        // remove product from list
        _storedProducts.RemoveAll(t => t.ID == product.ID);
        // store final product part on workshop so it can be removed by a worker
        _storedPart = product.GetPart();
        _storedPart.SetFinish(Quality.Normal);
        _storedPart.AddAmount(1);
        _storedPart.UnReserveTiles();
        _storedPart.UnReserveServiceTiles();
        Empty = false;
        
        // Being operations to make workshop avaliable again
        Messenger<Manufacturer>.Broadcast("new ManufacturerRequestWithdraw",this);
    }
    
    public Part WithdrawPart()
    {
        if (!Empty)
        {
            Empty = true;
            var temp = _storedPart;
            _storedPart = null;
            if(!_multipleProduction) SetManufacturerState(ManufacturerState.Waiting);
            return temp;
        }
        else
        {
            return null;
        }
    }

    public bool IsMultipleProduction()
    {
        return _multipleProduction;
    }
    
    public void StoreTiles(GridTile[] tiles)
    {
        if (_tiles != null)
        {
 
            
            GridTile[] oldTiles = _tiles;
            _tiles = new GridTile[oldTiles.Length + tiles.Length];
            Array.Copy(oldTiles, _tiles, oldTiles.Length);
            Array.Copy(tiles, 0, _tiles, oldTiles.Length, tiles.Length);
        }
        else
        {
            _tiles = tiles;
        }
    }
    
    public override void Remove()
    {
        base.Remove();
        Object.Destroy(_uiGameObject);
    }    
}
