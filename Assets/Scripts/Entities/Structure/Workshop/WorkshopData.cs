﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WorkshopDataList
{
    public List<WorkshopData> dataList;

    public WorkshopDataList()
    {
        dataList = new List<WorkshopData>();
    }

    public WorkshopData GetWorkshop(ManufacturerType type)
    {
        foreach (var data in dataList)
        {
            if (data.GetWorkshopType() == type)
            {
                return data;
            }
        }
        return dataList[0];
    }
}

[Serializable]
public struct WorkshopData  {

    [SerializeField]
    private string _name;
    [SerializeField]
    private ManufacturerType _type;
    [SerializeField]
    private string _description;
    [SerializeField]
    private PartType[] _workshopParts;
    [SerializeField]
    private PartType[] _workshopProducts;
    [SerializeField]
    private ModuleType[] _workshopModules;
    // Constructors

    public WorkshopData(string workshopName, ManufacturerType workshopType, PartType[] parts, PartType[] products, ModuleType[] modules, string desc)
    {
        _name = workshopName;
        _type = workshopType;
        _workshopParts = parts;
        _workshopProducts = products;
        _workshopModules = modules;
        _description = desc;
    }
    
    // Getters
    
    public string GetDescription()
    {
        return _description;
    }

    public string GetName()
    {
        return _name;
    }

    public ManufacturerType GetWorkshopType()
    {
        return _type;
    }

    public PartType[] GetWorkshopParts()
    {
        return _workshopParts;
    }
    
    public PartType[] GetWorkshopProducts()
    {
        return _workshopProducts;
    }
    
    public ModuleType[] GetWorkshopModules()
    {
        return _workshopModules;
    }
}
