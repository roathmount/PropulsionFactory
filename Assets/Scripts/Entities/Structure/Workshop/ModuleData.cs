﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ModuleDataList
{
    public List<ModuleData> DataList;

    public ModuleDataList()
    {
        DataList = new List<ModuleData>();
    }

    public ModuleData GetModule(ModuleType type)
    {
        foreach (var data in DataList)
        {
            if (data.GetModuleType() == type)
            {
                return data;
            }
        }
        return DataList[0];
    }
}

[Serializable]
public struct ModuleData {
    
    [SerializeField] private string _name;
    [SerializeField] private int _health;
    [SerializeField] private ModuleType _type;
    [SerializeField] private Vector2 _size;
    [SerializeField] private string _description;

    
    // Constructors

    public ModuleData(string name, int health, ModuleType type, Vector2 size, string description)
    {
        _name = name;
        _health = health;
        _type = type;
        _size = size;
        _description = description;
    }
    
    // Getters
    
    public int GetHealth()
    {
        return _health;
    }

    public ModuleType GetModuleType()
    {
        return _type;
    }
    
    public string GetName()
    {
        return _name;
    }
    
    public Vector2 GetSize()
    {
        return _size;
    }
	
    public string GetDescription()
    {
        return _description;
    }

    // Setters
    
    public void SetName(string name)
    {
        _name = name;
    }
    
    public void SetHealth(int amount)
    {
        _health = amount;
    }

    public void SetModuleType(ModuleType type)
    {
        _type = type;
    }
    
    public void SetSize(int sizeX,int sizeY)
    {
        _size.Set(sizeX,sizeY);
    }

    public void SetDescription(string desc)
    {
        _description = desc;
    }

    // Methods

}