﻿

public abstract class GridObject
{
	public Point Location;
	private GridObjectType _gridObjectType;
	public int X => Location.X;
	public int Y => Location.Y;
	public int ID;

	protected GridObject(int id, Point location, GridObjectType gridObjectType)
	{
		ID = id;
		_gridObjectType = gridObjectType;
		Location = location;
	}

	protected GridObject(int id, int x, int y, GridObjectType gridObjectType) : this(id, new Point(x, y), gridObjectType) {}

	public override string ToString()
	{
		return $"({X}, {Y})";
	}

	public GridObjectType GetGridObjectType()
	{
		return _gridObjectType;
	}

	public void SetGridObjectType(GridObjectType value)
	{
		_gridObjectType = value;
	}
	
}
