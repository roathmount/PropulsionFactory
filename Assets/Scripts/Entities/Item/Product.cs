﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A product is a combination of multiple parts, hence why it has a recipe stored inside, when all the parts are
// finally added to the product, this is converted to a part (stored already at the moment of creation).
public class Product : GridObject
{
    private Part _part;
    private Recipe _recipe;
    private GameObject _body;
    
    private int _partProgress;
    private bool _finished, _waitingResources;
    
    // TODO: product data

    public Product(int id, int x, int y, Part part, Recipe recipe) : base(id, x, y, GridObjectType.Product)
    {
        _part = part;
        _recipe = recipe;
        _partProgress = 0;
        foreach (var item in _recipe.GetItems())
        {
            _partProgress += item.GetItemAmount();
        }
        _finished = false;
        _waitingResources = false;
    }

    public Recipe GetRecipe()
    {
        return _recipe;
    }

    public Part GetPart()
    {
        return _part;
    }
    
    public int GetProgress()
    {
        return _partProgress;
    }

    public bool IsFinished()
    {
        return _finished;
    }
    
    public bool IsWaitingResources()
    {
        return _waitingResources;
    }
    
    public void SetWaitingResources(bool value)
    {
        _waitingResources = value;
    }
    
    public void AddProgress(int amount)
    {
        if ((_partProgress - amount) <= 0)
        {
            _partProgress = 0;
            _finished = true;
        }
        else
        {
            _partProgress -= amount; 
        }
    }
    
}
