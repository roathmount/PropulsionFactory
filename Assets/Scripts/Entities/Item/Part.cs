﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class Part : GridObject
{
	private Quality _qualityLevel;

	private GameObject _gameObject;

	private PartData _data;

	private int _partAmount, _reservedAmount;

	private bool _finished;
	private bool _queued;

	protected List<GridTile> _tiles;
	protected List<GridTile> _serviceTiles;
	
	// Constructors

	public Part(int x, int y) : base(0, x, y, GridObjectType.Part)
	{
		_data = new PartData("Part", 100, 1, PartType.None, false, new Vector2(0,0), "No description");

		_partAmount = 0;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = null;
		_finished = false;
		_queued = false;
	}

	public Part(PartData data, int id, int x, int y) : base(id, x, y, GridObjectType.Part)
	{
		_data = data;
		
		if(data.IsResource()) {SetGridObjectType(GridObjectType.Resource);}
		_partAmount = 0;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = null;
		_finished = false;
		_queued = false;
	}
	
	public Part(PartData data, int id, int x, int y, int amount) : base(id, x, y, GridObjectType.Part)
	{
		_data = data;
		
		if(data.IsResource()) {SetGridObjectType(GridObjectType.Resource);}
		_partAmount = amount;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = null;
		_finished = false;
		_queued = false;
	}
	
	public Part(PartData data, int x, int y, GameObject body) : base(body.GetInstanceID(), x, y, GridObjectType.Part)
	{
		_data = data;
		
		if(data.IsResource()) {SetGridObjectType(GridObjectType.Resource);}
		_partAmount = 0;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = body;
		_finished = false;
		_queued = false;
	}
	
	public Part(PartData data, int x, int y, int amount, GameObject body) : base(body.GetInstanceID(), x, y, GridObjectType.Part)
	{
		_data = data;
		
		if(data.IsResource()) {SetGridObjectType(GridObjectType.Resource);}
		_partAmount = amount;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = body;
		_finished = false;
		_queued = false;
	}

	public Part(int x, int y, GameObject body, int health, int amountLimit, PartType type, bool isResource, Vector2 size) : base(body.GetInstanceID(), x, y, GridObjectType.Part)
	{
		_data = new PartData(type.ToString(), amountLimit, health, type, isResource, size, "No description");
		
		if(isResource) {SetGridObjectType(GridObjectType.Resource);}
		_partAmount = 0;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = body;
		_finished = false;
		_queued = false;
	}
	
	public Part(string name, int x, int y, GameObject body, int health, int amountLimit, PartType type, bool isResource, Vector2 size, string description) : base(body.GetInstanceID(), x, y, GridObjectType.Part)
	{
		_data = new PartData(name, health, amountLimit, type, isResource, size, description);
		
		if(isResource) {SetGridObjectType(GridObjectType.Product);}
		_partAmount = 0;
		_reservedAmount = 0;
		_qualityLevel = Quality.Unknown;
		_gameObject = body;
		_finished = false;
		_queued = false;
	}
	
	// Getters

	public Quality GetQualityLevel()
	{
		return _qualityLevel;
	}

	public GameObject GetBody()
	{
		return _gameObject;
	}

	public bool IsFinished()
	{
		return _finished;
	}
	
	// Returns true if this item is already tasked for recharge
	public bool IsQueued()
	{
		return _queued;
	}

	public PartData GetPartData()
	{
		return _data;
	}

	public int GetPartAmount()
	{
		return _partAmount;
	}
	
	public int GetReservedAmount()
	{
		return _reservedAmount;
	}
	
	// Setters

	public void SetFinish(Quality qual)
	{
		if (!_gameObject.activeSelf)
		{
			_gameObject.SetActive(true);
		}
		_qualityLevel = qual;
		_finished = true;
	}
	
	public void SetQueued(bool value)
	{
		_queued = value;
	}
	
	public int AddAmount(int amount)
	{
		if ((_partAmount + amount) <= _data.GetAmountLimit())
		{
			_partAmount += amount;
			return 0;
		}
        
		if (_partAmount != _data.GetAmountLimit())
		{
			int leftover = _data.GetAmountLimit() - _partAmount;

			_partAmount += leftover;

			return amount - leftover;
		}
        
		return amount;
	}
	
	public int ReserveAmount(int amount)
	{
		if (_partAmount - amount >= 0)
		{
			_reservedAmount += UsePart(amount);
			return 0;
		}
		int currentReservedAmount = UsePart(amount);
		_reservedAmount += currentReservedAmount;
		int unreservedAmount = amount - currentReservedAmount;
		return unreservedAmount;
	}
	
	public int UseReserve(int amount)
	{
		_reservedAmount -= amount;
		if (_reservedAmount <= 0 && _partAmount <= 0)
		{
			DestroyGameObject();
		}
		return amount;
	}

	public int UsePart(int amount)
	{
		if ((_partAmount - amount) > 0)
		{
			_partAmount -= amount;
			return amount;
		}
		int tempAmount = _partAmount;
		_partAmount = 0;
		return tempAmount;
	}
	
	// Methods
	
	public void DoDamage(int amount)
	{
		if ((_data.GetHealth() - amount) > 0)
		{
			_data.SetHealth(_data.GetHealth()- amount);
		} else if ((_data.GetHealth()-amount) <= 0)
		{
			_data.SetHealth(0);
		}
	}
	
	// Adds to the ID list the tiles received
	public void ReserveTile(GridTile tile)
	{
		if (_tiles == null)
		{
			_tiles = new List<GridTile>();
		}
		tile.Passable = false;
		_tiles.Add(tile);
	}

	public void ReserveServiceTile(GridTile tile)
	{
		if (_serviceTiles == null)
		{
			_serviceTiles = new List<GridTile>();
		}
		tile.Service = true;
		_serviceTiles.Add(tile);
	}
	
	// Inverts X and Y cords of the machine if its rotated
	public void InvertSize()
	{
		var tempX = _data.GetSize().y;
		var tempY = _data.GetSize().x;

		_data.SetSize((int)tempX,(int)tempY);
	}

	public void UnReserveTiles()
	{
		if (_tiles == null) return;
		foreach (var tile in _tiles)
		{
			tile.Passable = true;
			tile.Reserved = false;
		}
		_tiles.Clear();
		_tiles = null;
	}
	
	public void UnReserveServiceTiles()
	{
		if (_serviceTiles == null) return;
		foreach (var tile in _serviceTiles)
		{
			tile.Service = false;
		}
		_serviceTiles.Clear();
		_serviceTiles = null;
	}
	
	public void DestroyGameObject()
	{
		Object.Destroy(_gameObject);
		_gameObject = null;
		UnReserveTiles();
		UnReserveServiceTiles();
		Messenger<Part>.Broadcast("new OnRemovePart",this);
	}
}
