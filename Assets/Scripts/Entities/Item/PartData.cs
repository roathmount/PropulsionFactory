﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class PartDataList
{
    public List<PartData> DataList;

    public PartDataList()
    {
        DataList = new List<PartData>();
    }

    public PartData GetPart(PartType type)
    {
        foreach (var data in DataList)
        {
            if (data.GetPartType() == type)
            {
                return data;
            }
        }
        return DataList[0];
    }
}

[Serializable]
public struct PartData {
    
    [SerializeField] private string _name;
    [SerializeField] private int _health;
    [SerializeField] private int _amountLimit;
    [SerializeField] private PartType _type;
    [SerializeField] private bool _isResource;
    [SerializeField] private Vector2 _size;
    [SerializeField] private string _description;

    
    // Constructors

    public PartData(string name, int health, int amountLimit, PartType type, bool isResource, Vector2 size, string description)
    {
        _name = name;
        _health = health;
        _amountLimit = amountLimit;
        _type = type;
        _isResource = isResource;
        _size = size;
        _description = description;
    }
    
    // Getters
    
    public int GetHealth()
    {
        return _health;
    }
    
    public int GetAmountLimit()
    {
        return _amountLimit;
    }

    public PartType GetPartType()
    {
        return _type;
    }

    public bool IsResource()
    {
        return _isResource;
    }
    
    public string GetName()
    {
        return _name;
    }
    
    public Vector2 GetSize()
    {
        return _size;
    }
	
    public string GetDescription()
    {
        return _description;
    }

    // Setters
    
    public void SetName(string name)
    {
        _name = name;
    }

    public void SetAmountLimit(int value)
    {
        _amountLimit = value;
    }
    
    public void SetHealth(int amount)
    {
        _health = amount;
    }

    public void SetPartType(PartType type)
    {
        _type = type;
    }

    public void SetResource(bool value)
    {
        _isResource = value;
    }
    
    public void SetSize(int sizeX,int sizeY)
    {
        _size.Set(sizeX,sizeY);
    }

    public void SetDescription(string desc)
    {
        _description = desc;
    }

    // Methods

}
