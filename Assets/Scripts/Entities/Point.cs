﻿
	using UnityEngine;

public struct Point
	{
		public int X, Y;
 
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public static float Distance(Point c1, Point c2)
		{
			Vector2 v1 = new Vector2(c1.X,c1.Y);
			Vector2 v2 = new Vector2(c2.X,c2.Y);

			float dist = Vector2.Distance(v1, v2);
			
			return dist;
		}
		
		public static bool operator ==(Point c1, Point c2) 
		{
			return c1.Equals(c2);
		}

		public static bool operator !=(Point c1, Point c2) 
		{
			return !c1.Equals(c2);
		}
	}
