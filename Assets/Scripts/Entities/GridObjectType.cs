﻿public enum GridObjectType
{
    Default,
    Machine,
    Stockpile,
    Workshop,
    Module,
    Product,
    Part,
    Resource,
    Worker
}

public enum ManufacturerType
{
    Default,
    BladeCut,
    SpinningFormer,
    MillingCnc,
    Compositer,
    Oven,
    ElectronicDischarge,
    WeldingRobot,
    FrictionStir,
    TubeBending,
    Dispenser,
    Stockpile,
    Hydraulic,
    Electronic,
    Assembly,
    Software,
    Recreation,
    MissionControl,
    Research
}

public enum ManufacturerState
{
    Idle,
    Working,
    Repairing,
    Waiting
}

public enum WorkerState
{
    Idle,
    Moving,
    OnTask,
    Operating,
    Done
}

public enum PartType
{
    None,
    RCS,
    Actuators,
    Avionics,
    CombustionChamber,
    COPV_Small,
    DanceFloor,
    Fairing_Small,
    Gridfin,
    Interstage_Small,
    Leg,
    LOXTank,
    LTS,
    Nozzle,
    Octaweb,
    PintleInjector,
    TankDome_Small,
    TankRing_Small,
    Turbopump,
    VacNozzle,
    Aluminium,
    Niobium,
    CarbonFiber,
    Sheets,
    Plates,
    Tubes,
    Pipes,
    Hydraulics,
    EngineSL
}

public enum ModuleType
{
    None,
    Desk,
    Chair,
    ToolCabinet
}

public enum PartCategory
{
    Default,
    Engine,
    Rocket,
    Capsule
}

public enum Quality
{
    Unknown,
    Awful,
    Poor,
    Normal,
    Good,
    Pristine
}