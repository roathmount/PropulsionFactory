﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Task {
    
    protected TaskAction[] Actions;

    private bool _finished;

    private TaskType _taskType;
    private TaskPriority _priority;

    private Worker _designatedWorker;

    public Task(TaskType taskType, TaskPriority priority)
    {
        _taskType = taskType;
        _priority = priority;
    }
    
    // Removes current action and returns next one
    public TaskAction UseNextAction()
    {
        if (Actions.Length > 0)
        {
            Actions = Actions.Where(val => val != GetCurrentAction()).ToArray();
            return GetCurrentAction();
        }
        else
        {
            _finished = true;
            return null;
        }
    }
    
    // Getters

    public TaskType GetTaskType()
    {
        return _taskType;
    }
    
    public TaskPriority GetPriority()
    {
        return _priority;
    }
    
    public Worker GetDesignatedWorker()
    {
        return _designatedWorker;
    }
    
    public TaskAction GetCurrentAction()
    {
        return Actions.Length > 0 ? Actions[0] : null;
    }
    
    public bool IsTaskFinished()
    {
        return _finished;
    }
    
    // Setters

    public void SetPriority(TaskPriority priority)
    {
        _priority = priority;
    }
    
    public void SetDesignatedWorker(Worker worker)
    {
        _designatedWorker = worker;
    }
    
}
