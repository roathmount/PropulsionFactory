﻿public enum TaskType
{
    Default,
    RechargeManufacturer,
    OperateManufacturer,
    TransportGridObject
}

public enum ActionTypes
{
    Default,
    Move,
    Gather,
    Deposit,
    Operate
}

public enum TaskPriority
{
    Low,
    Normal,
    High,
    Urgent
}