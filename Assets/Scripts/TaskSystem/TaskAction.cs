﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType {
    Move,
    Gather,
    Deposit,
    Operate
}

public class TaskAction {

    private GridObjectType _gridObjectType;
    private int _gridObjectID;
    private int _amount;

    private readonly ActionType _actionType;
    
    private bool _finished;
    
    private readonly Point _destination;

    // Move action constructor
    
    public TaskAction(ActionType actionType,Point destination)
    {
        _actionType = actionType;
        _destination = destination;
        _finished = false;
        _gridObjectID = 0;
        _amount = 0;
    }
    
    // Gather/Deposit action constructors
    
    public TaskAction(ActionType actionType, GridObjectType gridObjectType, Point destination)
    {
        _actionType = actionType;
        _gridObjectType = gridObjectType;
        _destination = destination;
        _finished = false;
    }
    
    // Methods
    
    // Getters
    
    public Point GetDestination()
    {
        return _destination;
    }

    public bool IsActionFinished()
    {
        return _finished;
    }

    public ActionType GetActionType()
    {
        return _actionType;
    }

    public int GetActionAmount()
    {
        return _amount;
    }
    
    // Return a gridObject to finish action
    public int GetGridObjectID()
    {
        return _gridObjectID;
    }

    public GridObjectType GetGridObjectType()
    {
        return _gridObjectType;
    }
    
    // Setters

    public void SetActionFinished()
    {
        _finished = true;
    }

    public void SetGridObjectID(int id)
    {
        _gridObjectID = id;
    }

    public void SetActionAmount(int value)
    {
        _amount = value;
    }
    
}
