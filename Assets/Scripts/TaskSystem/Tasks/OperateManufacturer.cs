﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperateManufacturer : Task
{
    public OperateManufacturer(TaskType taskType, TaskPriority priority, Manufacturer manufacturer) : base(taskType, priority)
    {
        Actions = new TaskAction[4];
        
        // Move to machine
        Actions[0] = new TaskAction(ActionType.Move, manufacturer.GetServiceTile().Location);
        // Operate machine
        Actions[1] = new TaskAction(ActionType.Operate, manufacturer.GetGridObjectType(), manufacturer.GetServiceTile().Location);
        Actions[1].SetGridObjectID(manufacturer.ID);
    }
}
