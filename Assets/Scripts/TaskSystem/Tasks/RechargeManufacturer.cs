﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeManufacturer : Task
{
   
    public RechargeManufacturer(TaskType taskType, TaskPriority priority, Manufacturer manufacturer, GridObject resource, int amount) : base(taskType, priority)
    {
        Actions = new TaskAction[4];
        
        // move to resource
        Actions[0] = new TaskAction(ActionType.Move, resource.Location);
        // gather resource
        Actions[1] = new TaskAction(ActionType.Gather, GridObjectType.Part, resource.Location);
        Actions[1].SetGridObjectID(resource.ID);
        Actions[1].SetActionAmount(amount);
        // move to machine
        Actions[2] = new TaskAction(ActionType.Move, manufacturer.GetServiceTile().Location);
        // deposit resource
        Actions[3] = new TaskAction(ActionType.Deposit, GridObjectType.Part, manufacturer.GetServiceTile().Location);
        Actions[3].SetGridObjectID(manufacturer.ID);
        Actions[3].SetActionAmount(amount);
    }

}

