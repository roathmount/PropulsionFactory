﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportGridObject : Task {
    
    public TransportGridObject(TaskType taskType, TaskPriority priority, Manufacturer manufacturer, GridTile destination) : base(taskType, priority)
    {
        Actions = new TaskAction[4];
        
        // Move to gridObject
        Actions[0] = new TaskAction(ActionType.Move,manufacturer.GetServiceTile().Location); 
        // Gather gridObject
        Actions[1] = new TaskAction(ActionType.Gather, manufacturer.GetGridObjectType(), manufacturer.GetServiceTile().Location);
        Actions[1].SetGridObjectID(manufacturer.ID);
        // Move to destination
        Actions[2] = new TaskAction(ActionType.Move, destination.Location);
        // Deposit gridObject
        Actions[3] = new TaskAction(ActionType.Deposit, GridObjectType.Part, destination.Location);
        Actions[3].SetGridObjectID(destination.ID);
    }
    
    public TransportGridObject(TaskType taskType, TaskPriority priority, GridTile destination) : base(taskType, priority)
    {
        Actions = new TaskAction[2];
        // Move to destination
        Actions[0] = new TaskAction(ActionType.Move, destination.Location);
        // Deposit gridObject
        Actions[1] = new TaskAction(ActionType.Deposit, GridObjectType.Part, destination.Location);
        Actions[1].SetGridObjectID(destination.ID);
    }
    
}
