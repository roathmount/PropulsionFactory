﻿using UnityEngine;
using System.Collections;
using System;

public class CameraWork : MonoBehaviour
{
 
    private Vector3 _lookAt;

    private bool _canMove;
    private bool _canTurbo;
    private bool _canRotate;
    private bool _canZoom;

    private float _rotation,_currRotation;
    
    private float _zoom, _currZoom;

    public float CurrZoom
    {
        get { return _currZoom; }
        set { _currZoom = value; }
    }

    private float _minZoom, _maxZoom;

    private float _movementSpeed;
    private float _turboSpeed;
    private float _rotateSpeed;
    private float _zoomSpeed;
    
    private Vector3 _minBounds;
    private Vector3 _maxBounds;

    private float _movementSmoothness;
    private float _rotationSmoothness;
    private float _zoomSmoothness;
    
    private Vector3 _moveVector;
    
    private GameObject _pivotPoint;
    
    public LayerMask CameraCollisionLayerMask;
        
    public void Start()
    {
        _lookAt = new Vector3(10, 0, 20);

        
        _canMove = true;
        _canTurbo = true;
        _canRotate = true;
        _canZoom = true;
        
        _movementSpeed = 20f;
        _turboSpeed = 40f;
        _rotateSpeed = 137.5f;
        _zoomSpeed = 500f;
        _movementSmoothness = 5f;
        
        _rotation = 0f;
        _rotationSmoothness = 5f;
        
        _minBounds = new Vector3(2, 0, 2);
        _maxBounds = new Vector3(GridManager.SizeX-2, 0, GridManager.SizeY-2);
        
        _zoom = 16f;
        _minZoom = 8f;
        _maxZoom = 32f;
        _zoomSmoothness = 5f;

        _currRotation = _rotation;
        _currZoom = _zoom;
        
        GeneratePivot();
    }

    private void Reset()
    {
        _lookAt = new Vector3(10, 0, 20);
        _movementSmoothness = 1f;

        _canMove = true;
        _canRotate = true;
        _canZoom = true;

        _movementSpeed = 20f;
        _rotateSpeed = 90f;
        _zoomSpeed = 500f;
        
        _rotation = 0f;
        _rotationSmoothness = 5f;
        
        _zoom = 16f;
        _minZoom = 4f;
        _maxZoom = 32f;
        _zoomSmoothness = 5f;
    }
    
    private void Update()
    {
        if (_canMove)
        {
            float currentSpeed = _movementSpeed;
            if (_canTurbo && Input.GetKey(KeyCode.LeftShift))
            {
                currentSpeed = _turboSpeed;
            }
            
            float hRaw = Input.GetAxisRaw("Horizontal");
            float vRaw = Input.GetAxisRaw("Vertical");

            if (Mathf.Abs(hRaw) > 0.001f)
            {
                MoveCameraPos(hRaw * currentSpeed * Time.deltaTime, 0, 0);
            }
            if (Mathf.Abs(vRaw) > 0.001f)
            {
                MoveCameraPos(0, 0, vRaw * currentSpeed * Time.deltaTime);
            }
        }
        
        if (_canRotate)
        {
            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetMouseButton(2))
            {
                float rot = Input.GetAxisRaw("Mouse X");
                AddRotation(rot * _rotateSpeed * Time.deltaTime);
            }
        }
        
        if (_canZoom)
        {
            if (_canTurbo && Input.GetKey(KeyCode.LeftShift))
            {
                float zoom = Input.GetAxis("Mouse ScrollWheel");
                AddZoom(-(zoom*50));
            }
            else
            { 
                float zoom = Input.GetAxisRaw("Mouse ScrollWheel");
                AddZoom(-(zoom * _zoomSpeed * Time.deltaTime));
            }
        }
    }

    protected void LateUpdate()
    {                
        _moveVector.y = 0;
        _lookAt += Quaternion.Euler(0, _rotation, 0) * _moveVector;
        _lookAt.y = 0;
        
        _lookAt = new Vector3(Mathf.Clamp(_lookAt.x, _minBounds.x, _maxBounds.x), Mathf.Clamp(_lookAt.y, _minBounds.y, _maxBounds.y), Mathf.Clamp(_lookAt.z, _minBounds.z, _maxBounds.z));
        _zoom = Mathf.Clamp(_zoom, _minZoom, _maxZoom);
        
        // Smoothing
        
        if (_canTurbo && Input.GetKey(KeyCode.LeftShift))
        {
            _currZoom = _zoom;
        }
        else
        {
            _currZoom = Mathf.Lerp(_currZoom, _zoom, Time.deltaTime * _zoomSmoothness);
        }
        _currRotation = Mathf.LerpAngle(_currRotation, _rotation, Time.deltaTime * _rotationSmoothness);
        _pivotPoint.transform.position = Vector3.Lerp(_pivotPoint.transform.position, _lookAt, Time.deltaTime * _movementSmoothness);

        _moveVector = Vector3.zero;
        
        UpdateCameraCollision();
        
        UpdateCamera();
    }
    
    private void UpdateCamera()
    {
        var rotation = Quaternion.Euler(45, _currRotation, 0);
        Vector3 v = new Vector3(0.0f, 0.0f, -_currZoom);
        Vector3 position = rotation * v + _pivotPoint.transform.position;
        
        
        var y = 1;
        if (y > position.y)
            position.y = y;

        transform.rotation = rotation;
        transform.position = position;
    }

    public void MoveCameraPos(float x, float y, float z)
    {
        _moveVector += new Vector3(x, y, z);
    }
    
    private void AddRotation(float amount)
    {
        _rotation += amount;
    }

    private void AddZoom(float amount)
    {
        _zoom += amount;
    }
    
    private void UpdateCameraCollision()
    {
        var direction = (transform.position - _pivotPoint.transform.position);
        direction.Normalize();

        var distance = _zoom;

        RaycastHit hitInfo;

        if (Physics.Raycast(_pivotPoint.transform.position, direction, out hitInfo, distance, CameraCollisionLayerMask))
        {
            if (hitInfo.transform != _pivotPoint)
            {
                _currZoom = hitInfo.distance - 0.1f;
            }
        }
    }
    
    private void GeneratePivot()
    {
        _pivotPoint = new GameObject("PivotPoint");
        _pivotPoint.transform.position = _lookAt;
        
        Collider targetCollider = _pivotPoint.GetComponent<Collider>();
        if (targetCollider != null)
        {
            targetCollider.enabled = false;
        }
    }
    
}
